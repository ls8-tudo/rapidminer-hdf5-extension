Changes in 5.5.5
----------------
* Changed minimum compatible Version to 6.5
* Adapted code to remove warnings
