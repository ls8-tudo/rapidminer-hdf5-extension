Changes in 5.5.7
----------------
* Fixed an issue when dragging HDF5 reader operator into a process
