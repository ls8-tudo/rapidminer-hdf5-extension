Changes in 5.5.3
----------------
* Adapted operator tree properties due to RM 7 changes
