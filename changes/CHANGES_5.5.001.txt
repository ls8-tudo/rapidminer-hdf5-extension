Changes in 5.5.1
----------------
* Added a parameter to HDF5Writer to select enum handling ([dynamic], only enum, only string)
* Changed HDF5Writer so the attribute roles would be preserved in the written example set
* Fixed an issue with group creation
* Fixed an issue related to enum data types where the enum member string would be too long (only dynamic handling)
* Fixed an issue related to the group parameter of HDF5Writer
