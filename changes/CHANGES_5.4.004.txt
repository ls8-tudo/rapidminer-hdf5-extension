Changes in 5.4.4
----------------
* Updated to JHDF version 2.11.0
* Fixed an issue with and improved on value manipulation