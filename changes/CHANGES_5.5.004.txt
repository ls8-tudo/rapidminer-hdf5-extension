Changes in 5.5.4
----------------
* Improved error handling for nonexistent or faulty data sets.
