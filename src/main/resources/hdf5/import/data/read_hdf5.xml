<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../documentation2html.xsl"?>
<p1:documents xmlns:p1="http://rapid-i.com/schemas/documentation/reference/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://rapid-i.com/schemas/documentation/reference/1.0                http://rapid-i.com/schemas/documentation/reference/1.0/documentation.xsd">
	<!-- each operator should have a key that consists of "operator." plus the operator's key. -->
	<operator key="operator.hdf5:read_hdf5" locale="en" version="5.4.001">
		<title>Read HDF5</title>
		<synopsis>This operator is used to read example sets from HDF5 files.</synopsis>
		<text>
			<paragraph>HDF5 (short for Hierarchical Data Format) is a widely used big data format developed by the HDF Group (http://www.hdfgroup.org). HDF is used when the data sets are very big or complex and still need to be accessed quickly. Due to its binary format, it allows for easy injection of compression (namely gzip) if necessary. An HDF file may contain several data sets that are organized by names and stored in groups in a tree much like in a file system.</paragraph>
			<paragraph>This operator is used to read some or all data sets from a given HDF5 file into RapidMiner example sets. Because this is a format transformation, there are some limitations. While it is possible to read scalar and compound data sets, it is not possible to map nested data sets to RapidMiner. Beneath you can find a mapping from HDF data types to the RapidMiner core ontology. Attributes will be named by convention &lt;data set name&gt;.&lt;attribute name&gt; to keep the context. Example sets will also be annoted with <em>source</em> and <em>file name</em>, representing the full path of the data set and the HDF file respectively.</paragraph>
			<paragraph>HDF5 data type | RapidMiner ontology
			<ul>
				<li>
					Integer, Char | Integer
				</li>
				<li>
					Float | Real
				</li>
				<li>
					String, Enum | [Poly|Bi]Nominal
				</li>
				<li>
					Time | Time
				</li>
				<li>
					Array, Vlen | Collection of attributes
				</li>
				<li>
					Bitfield, Compound, no Class, Opaque, Reference | Not supported and thus ignored
				</li>
			</ul></paragraph>
			<paragraph>If an array or variable length data type is present, it will be converted to a collection of attributes. E.g. An array type <em>att</em> of size 3x4 will be represented as 12 distinct attributes of the form <em>att[0][0]</em> through <em>att[2][3]</em>. The attribute type corresponds to the base type of the array.</paragraph>
			<paragraph>One advantage of HDF is that a data set is only materialized in memory if it is needed, thus it is very memory efficient. To use this to full capacity, the default way to generate an example set is by referencing the data set and materializing examples on demand. This restricts write access to these example sets, allowing only for reading examples and removing attributes (e.g. with the Select Attributes operator). Creating/generating new attributes is not possible in this setting.</paragraph>
			<paragraph>To materialize example sets completely in memory, check the respective parameter. This will cost more memory but allows for full example set manipulation.</paragraph>
			<paragraph>If multiple data sets are correlated and you need to join them, it is possible to select attributes that should not have the data set name as a prefix to allow a join by keys. The operator does not join the example sets automatically to allow attribute selection beforehand. If a join is intended, it is not necessary to materialize the data sets in memory since the Join operator creates a completely materialized example set. For joining, have a look at the HDF5 join sample process.</paragraph>
			<paragraph>For complete understanding of this operator read the parameters section thoroughly.</paragraph>
		</text>
		<inputPorts>
			<port name="file" type="com.rapidminer.operator.nio.file.FileObject">An HDF5 file is expected as a file object which can be created with other operators with file output ports like the Read File operator.
			</port>
		</inputPorts>
		<outputPorts>
			<port name="output" type="com.rapidminer.example.ExampleSet">This port delivers the selected data set(s) from the HDF5 file. This can either be a single example set or a collection of those.</port>
		</outputPorts>
		<parameters>
			<parameter key="hdf5_file" type="string">The path of the HDF5 file is specified here. It can be selected using the <em>choose a file</em> button.</parameter>
			<parameter key="table_filter_type" type="selection" default="single">This parameter allows you to select the table selection filter; the method you want to use for selecting tables. It has the following options:
				<values>
					<value value="single">This option allows the selection of a single data set. When this option is selected another parameter (table) becomes visible in the Parameter View.</value>
					<value value="subset">This option allows the selection of multiple data sets through a list. All data sets of the HDF file are present in the list; required tables can be easily selected. When this option is selected the parameters <em>tables</em> and <em>invert</em> will become visible in the Parameter View.</value>
					<value value="all">This option simply selects all data sets of the HDF file.</value>
			</values></parameter>
			<parameter key="table" type="string">The required table can be selected from this option. The table name can be selected from the drop down box of the <em>parameter</em> table if the meta data is known.</parameter>
			<parameter key="tables" type="string">The required tables can be selected from this option. This opens a new window with two lists. All data sets of the HDF file are present in the left list and can be shifted to the right list, which is the list of selected tables that will be provided at the output port.</parameter>
			<parameter key="invert" type="boolean">If this parameter is set to true, it acts as a NOT gate, it reverses the selection of tables.</parameter>
			<parameter key="join_attributes" type="string">The required attributes for joining can be selected from this option. This opens a new window with two lists. On the left will be a list with all attributes that are present in all (compound) data sets of the HDF file. You can easily shift the required attributes to the right side. If you only need a subset of the data sets, you may add other attributes to the right side by typing in the name and pressing the green "+" icon.</parameter>
			<parameter key="in_memory" type="boolean">If this parameter is set to true, the data set(s) will become completely materialized in memory for full example set manipulation. The default value is false to reduce memory consumption.</parameter>
		</parameters>

		<relatedDocuments>
			<!-- ... -->
		</relatedDocuments>

	</operator>
</p1:documents>