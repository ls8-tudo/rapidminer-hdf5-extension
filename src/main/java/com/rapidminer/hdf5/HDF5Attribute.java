/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import ncsa.hdf.object.Dataset;

import com.rapidminer.example.Attribute;

/**
 * A mapper class to bridge RapidMiner's {@link Attribute} to the HDF5 style
 * {@link ncsa.hdf.object.Attribute Attribute}. Consists of the index of the
 * original HDF attribute in the {@link Dataset}, subindices if the original
 * attribute was an array type, the total order of the attribute and in case of
 * a nominal attribute, the mapping between RapidMiner and HDF enum indices.
 * 
 * @author Jan Czogalla
 * 
 */
public class HDF5Attribute implements Serializable {

	private static final long serialVersionUID = -3003938095081897862L;

	/**
	 * The index of the member in the dataset, followed by subinidces if the
	 * type is an array
	 */
	private int[] index;
	/**
	 * The order of the attribute
	 */
	private int order;
	/**
	 * The mapping between RapidMiner and HDF enum indices
	 */
	private List<Integer> RM2HDF;

	/**
	 * Creates an attribute link with the specified index/subindices, the order
	 * and the nominal mapping.
	 * 
	 * @param index
	 *            - The index of the member, followed by subindices.
	 * @param order
	 *            - The order of the attribute.
	 * @param RM2HDF
	 *            - The nominal mapping.
	 */
	public HDF5Attribute(int[] index, int order, List<Integer> RM2HDF) {
		this.index = index;
		this.order = order;
		this.RM2HDF = RM2HDF;
	}

	/**
	 * @see #HDF5Attribute(int[], int, List)
	 */
	public HDF5Attribute(int[] index, List<Integer> RM2HDF) {
		this(index, 0, RM2HDF);
	}

	/**
	 * @see #HDF5Attribute(int[], int, List)
	 */
	public HDF5Attribute(int[] index, int order) {
		this(index, order, null);
	}

	/**
	 * @see #HDF5Attribute(int[], int, List)
	 */
	public HDF5Attribute(int[] index) {
		this(index, 0);
	}

	/**
	 * Returns true if the attribute this is based on is an array type.
	 * 
	 * @return true if the attribute is an array type.
	 */
	public boolean isArray() {
		return index.length > 1;
	}

	/**
	 * Returns true if a nominal mapping exists, meaning that the base attribute
	 * type was string or enum.
	 * 
	 * @return true if a nominal mapping exists.
	 */
	public boolean isPolynominal() {
		return RM2HDF != null;
	}

	/**
	 * Returns the index of the member in the actual {@link Dataset}.
	 * 
	 * @return the index of the member.
	 */
	public int getIndex() {
		return index[0];
	}

	/**
	 * Creates and returns the subindices if this is an array type attribute,
	 * based on the current example index.
	 * 
	 * @param row
	 *            - The example index.
	 * @return the subindices for this attribute.
	 */
	public int[] getSubindices(int row) {
		if (!isArray()) {
			return new int[1];
		}
		int[] subIndices = Arrays.copyOfRange(index, 1, index.length);
		subIndices[0] += row * order;
		return subIndices;
	}

	/**
	 * Set the indices for this HDF5 backed attribute. This is only
	 * necessary/allowed during remapping attributes when creating a temporary
	 * HDF5 data set.
	 * 
	 * @param index
	 *            The new indices.
	 */
	protected void setIndex(int[] index) {
		this.index = index;
	}

	/**
	 * Maps the HDF enum index to the RapidMiner enum index.
	 * 
	 * @param index
	 *            - The HDF enum index.
	 * @return the RapidMiner enum index.
	 */
	public int map2RM(int index) {
		if (RM2HDF != null) {
			return RM2HDF.indexOf(index);
		}
		return index;
	}

	/**
	 * Maps the RapidMiner enum index to the HDF enum index.
	 * 
	 * @param index
	 *            - The RapidMiner enum index.
	 * @return the HDF enum index.
	 */
	public int map2HDF(int index) {
		if (RM2HDF != null) {
			return RM2HDF.get(index);
		}
		return index;

	}

}
