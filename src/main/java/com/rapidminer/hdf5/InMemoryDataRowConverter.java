/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import com.rapidminer.example.table.AbstractDataRowReader;
import com.rapidminer.example.table.DataRow;
import com.rapidminer.example.table.DataRowReader;
import com.rapidminer.example.table.DoubleArrayDataRow;

/**
 * This data row reader converts the {@link HDF5DatasetDataRow}s of an
 * {@link HDF5DatasetDataRowReader} to modifiable {@link DoubleArrayDataRow}s.
 * 
 * @author Jan Czogalla
 * 
 */
public class InMemoryDataRowConverter implements DataRowReader {

	/**
	 * The original HDF5 backed data row reader
	 */
	private HDF5DatasetDataRowReader reader;

	public InMemoryDataRowConverter(HDF5DatasetDataRowReader reader) {
		this.reader = reader;
	}

	@Override
	public boolean hasNext() {
		return reader.hasNext();
	}

	@Override
	public DataRow next() {
		return ((HDF5DatasetDataRow) reader.next()).toBasicDataRow();
	}

	/**
	 * @see AbstractDataRowReader#remove()
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException(
				"The method 'remove' is not supported by DataRowReaders!");
	}
}
