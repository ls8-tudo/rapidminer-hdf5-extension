/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import java.lang.reflect.Field;

import com.rapidminer.example.Tools;
import com.rapidminer.example.table.AbstractSparseArrayDataRow;
import com.rapidminer.example.table.DataRow;
import com.rapidminer.example.table.DataRowFactory;
import com.rapidminer.example.table.DoubleSparseArrayDataRow;

/**
 * A specialization of {@link DoubleSparseArrayDataRow} for
 * {@link HDF5DatasetExampleTable HDF5DatasetExampleTables}. It has a reference
 * to the underlying {@link HDF5DatasetExampleTable} to handle data
 * manipulation.
 * 
 * @author Jan Czogalla
 * 
 */
public class HDF5DatasetDataRow extends DoubleSparseArrayDataRow {

	private static final long serialVersionUID = -6571726101375890267L;
	private int row;
	private HDF5DatasetExampleTable table;

	public HDF5DatasetDataRow(int[] indices, double[] data, int row,
			HDF5DatasetExampleTable table) {
		super(indices.length);
		this.row = row;
		this.table = table;
		// counter is private, not protected
		try {
			Field counter = AbstractSparseArrayDataRow.class
					.getDeclaredField("counter");
			counter.setAccessible(true);
			counter.set(this, indices.length);
		} catch (Exception e) {
			throw new RuntimeException("Could not create data row", e);
		}
		System.arraycopy(indices, 0, getNonDefaultIndices(), 0, indices.length);
		System.arraycopy(data, 0, getAllValues(), 0, data.length);
	}

	@Override
	protected void set(int index, double value, double defaultValue) {
		if (Tools.isDefault(value, get(index, defaultValue))) {
			// no change
			return;
		}
		super.set(index, value, defaultValue);
		table.set(row, index, value);
	}

	@Override
	public int getType() {
		// in conjunction with HDF5Table extending MemoryTable, preventing
		// materialisation of data set in memory (see
		// AbstractExampleSetProcessing)
		return DataRowFactory.TYPE_SPECIAL;
	}

	/**
	 * Convert this HDF5 backed data row to an unlinked basic
	 * {@link DoubleSparseArrayDataRow}.
	 * 
	 * @return an unlinked {@link DoubleSparseArrayDataRow} copy of this data
	 *         row.
	 */
	public DataRow toBasicDataRow() {
		int size = getNonDefaultIndices().length;
		DoubleSparseArrayDataRow dr = new DoubleSparseArrayDataRow(size);
		// counter is private, not protected
		try {
			Field counter = AbstractSparseArrayDataRow.class
					.getDeclaredField("counter");
			counter.setAccessible(true);
			counter.set(dr, size);
		} catch (Exception e) {
			throw new RuntimeException("Could not create data row", e);
		}
		System.arraycopy(getNonDefaultIndices(), 0, dr.getNonDefaultIndices(),
				0, size);
		System.arraycopy(getAllValues(), 0, dr.getNonDefaultValues(), 0, size);
		return dr;
	}

}
