/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import java.io.File;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;

import javax.swing.tree.DefaultMutableTreeNode;

import ncsa.hdf.object.CompoundDS;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.Group;
import ncsa.hdf.object.ScalarDS;
import ncsa.hdf.object.h5.H5File;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.SimpleExampleSet;
import com.rapidminer.example.table.AttributeFactory;
import com.rapidminer.example.table.BinominalMapping;
import com.rapidminer.example.table.DataRow;
import com.rapidminer.example.table.DataRowReader;
import com.rapidminer.example.table.ExampleTable;
import com.rapidminer.example.table.MemoryExampleTable;
import com.rapidminer.example.table.NominalAttribute;
import com.rapidminer.example.table.NominalMapping;
import com.rapidminer.example.table.PolynominalMapping;
import com.rapidminer.hdf5.util.Compatiblity;
import com.rapidminer.hdf5.util.ConverterUtil;
import com.rapidminer.operator.Annotations;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.tools.Ontology;

/**
 * This {@link ExampleTable} provides access to an HDF 5 {@link Dataset}. This
 * minimizes memory consumption at runtime, especially for very large data sets.
 * Whenever possible, the connection to the data set will be cut to prevent a
 * memory overflow. Values can be manipulated and new attributes can be added
 * and the result can be written through usual RapidMiner means. Also, the
 * changes may be written to a temporary file and read from there again if the
 * number of changes exceed a certain amount of memory.
 * 
 * @author Jan Czogalla
 * 
 */
public class HDF5DatasetExampleTable extends MemoryExampleTable implements
		Cloneable {

	private static final long serialVersionUID = 7928440078687627319L;
	// maximal number of double values "allowed" in cache
	static final int CACHE_MEMORY = 125000000;
	// ratio window size : cache size
	static final double WINDOW_CACHE_RATIO = .75;
	// ratio of window size to look into the future for preloading
	private static final double PRELOAD_RATIO = .25;
	// the data set to read from
	Dataset dataset;
	// the read lock
	private final long[] locks = new long[1];
	private boolean inMemory = false;
	private long lastAccess;
	private DateFormat df;
	// mapping of RapidMiner attributes to HDF attributes
	SortedMap<Integer, HDF5Attribute> h5AttMapping;
	// removed and not replaced HDF 5 attributes
	private List<HDF5Attribute> removedAtts;
	// newly added attributes (may replace a removed HDF 5 attribute)
	SortedMap<Integer, double[][]> newAttributes;
	// changed values of HDF 5 backed attributes
	Map<Integer, double[][]> newValues;
	List<Integer> compatible;
	// used member IDs of HDF 5 data set
	List<Integer> usedMembers = new ArrayList<Integer>();
	// array members of HDF 5 data set by ID
	private Map<Integer, Integer> arrayMembers;
	boolean isComp;
	private boolean isTemp = false;
	private int size;
	// maximum number of examples in cache
	private int maxCacheSize;
	// window size dependent on max cache size (3/4)
	int windowSize;
	long chunkSize = -1;
	// current position for window
	int currentWindowStart = 0;
	// current maximum data row index loaded
	private int maxLoadedIndex = -1;
	// row cache to minimize main memory access
	private LinkedHashMap<Integer, SoftReference<DataRow>> rowCache = new LinkedHashMap<Integer, SoftReference<DataRow>>() {

		private static final long serialVersionUID = 2793003887032957782L;

		@Override
		public SoftReference<DataRow> put(Integer key,
				SoftReference<DataRow> value) {
			if (value == null || value.get() == null) {
				return null;
			}
			return super.put(key, value);
		}

		/**
		 * Returns <tt>true</tt> if this map contains a still referenced mapping
		 * for the specified key.
		 * 
		 * @param key
		 *            {@inheritDoc}
		 * 
		 * @return <tt>true</tt> if this map contains a still referenced mapping
		 *         for the specified key.
		 */
		@Override
		public boolean containsKey(Object key) {
			return get(key) != null;
		}

		/**
		 * Returns the value to which the specified key is mapped, or
		 * {@code null} if this map contains no mapping for the key or the
		 * mapping is not referenced anymore.
		 * 
		 * @param key
		 *            {@inheritDoc}
		 * @return the value to which the specified key is mapped, or
		 *         {@code null} if this map contains no mapping for the key or
		 *         the mapping is not referenced anymore.
		 */
		@Override
		public SoftReference<DataRow> get(Object key) {
			SoftReference<DataRow> value = super.get(key);
			if (value == null) {
				return null;
			}
			// check if still referenced
			if (value.get() == null) {
				// not referenced => remove
				remove(key);
				return null;
			}
			return value;
		}

		@Override
		protected boolean removeEldestEntry(
				Map.Entry<Integer, SoftReference<DataRow>> eldest) {
			// remove if not referenced anymore
			return eldest != null
					&& (eldest.getValue() == null || eldest.getValue().get() == null);
		}
	};
	// timer and timer task for freeing data set memory
	private Timer clearTimer = new Timer(true), updateTimer;
	private TimerTask clearer;
	private final boolean[] cancelled = new boolean[] { true };

	/**
	 * @see #HDF5DatasetExampleTable(Dataset, List)
	 */
	public HDF5DatasetExampleTable(Dataset dataset) throws OperatorException {
		this(dataset, new ArrayList<String>());
	}

	/**
	 * Creates a new example table based on the given {@link Dataset}. If
	 * attribute names are provided in the join list, those attributes will not
	 * have the data set's name as a prefix.
	 * 
	 * @param dataset
	 *            - The data set to read from.
	 * @param joinAtts
	 *            - The attribute names that should not get a prefix.
	 * @throws OperatorException
	 *             if the data set is null or the type is not supported.
	 */
	@SuppressWarnings("deprecation")
	public HDF5DatasetExampleTable(Dataset dataset, List<String> joinAtts) throws OperatorException {
		super(new ArrayList<Attribute>());
		if (dataset == null) {
			throw new OperatorException("Data set must not be null.");
		}
		this.dataset = dataset;
		if (dataset instanceof CompoundDS) {
			isComp = true;
			createAttributes((CompoundDS) dataset, joinAtts);
		} else if (dataset instanceof ScalarDS) {
			isComp = false;
			createAttributes((ScalarDS) dataset);
		} else {
			throw new OperatorException("Dataset type " + dataset.getClass()
					+ " of data set " + dataset.getFullName()
					+ " not supported.");
		}
		if (getAttributeCount() == 0) {
			throw new OperatorException(
					"No valid attributes found in data set "
							+ dataset.getFullName());
		}
		size = (int) dataset.getDims()[0];
		if (dataset.getChunkSize() != null) {
			chunkSize = dataset.getChunkSize()[0];
		}
		updateCacheSize();
		dataset.getStride();
		startUpdateTimer();
	}

	/**
	 * Creates the attributes for the given {@link ScalarDS}. Will create
	 * multiple attributes if the data set is multi-dimensional.
	 * 
	 * @param scalar
	 *            - The scalar data set.
	 */
	private void createAttributes(ScalarDS scalar) {
		scalar.init();
		try {
			scalar.getMetadata();
		} catch (Exception e) {
		}
		h5AttMapping = new TreeMap<Integer, HDF5Attribute>();
		String attName = scalar.getName();
		Datatype attType = scalar.getDatatype();
		int order = 1;
		// compute order if the data set has more than one dimension
		long[] dims = scalar.getDims();
		for (int i = 1; i < dims.length; i++) {
			order *= dims[i];
		}
		addAttribute(attType, attName, order, 0);
		scalar.clear();
	}

	/**
	 * Creates the attributes for the given {@link CompoundDS}. Will create
	 * multiple RapidMiner attributes for array type HDF attributes.
	 * 
	 * @param comp
	 *            - The compound data set.
	 * @param joinAtts
	 *            - The list of attributes without prefix.
	 */
	private void createAttributes(CompoundDS comp, List<String> joinAtts) {
		comp.init();
		try {
			comp.getMetadata();
		} catch (Exception e) {
		}
		h5AttMapping = new TreeMap<Integer, HDF5Attribute>();
		// meta data for all attributes
		String[] attNames = comp.getMemberNames();
		Datatype[] attTypes = comp.getMemberTypes();
		int[] attOrders = comp.getMemberOrders();
		String setName = dataset.getFullName().substring(1).replace('/', '.');
		Datatype type;
		String name;
		int order;
		// deselect all members
		// might be selected during enum creation from String data types but
		// will be reset
		comp.setMemberSelection(false);
		// add all attributes
		for (int i = 0; i < attNames.length; i++) {
			type = attTypes[i];
			name = attNames[i];
			order = attOrders[i];
			if (!joinAtts.contains(name)) {
				name = setName + "." + name;
			}
			addAttribute(type, name, order, i);
		}
		// select all actually used members
		for (int i : usedMembers) {
			comp.selectMember(i);
		}
		comp.clear();
	}

	/**
	 * Adds a single attribute from the {@link Dataset}. Will create multiple
	 * RapidMiner attributes if an array type is present. An HDF attribute
	 * <code>att</code> of type integer array with dimensions {3,4} will be
	 * represented in RapidMiner as 12 attributes in the form of
	 * <code>att[0][0]</code> through <code>att[2][3]</code>. <br/>
	 * Some attribute types are not supported, namely
	 * <ul>
	 * <li>{@link Datatype#CLASS_BITFIELD}</li>
	 * <li>{@link Datatype#CLASS_COMPOUND}</li>
	 * <li>{@link Datatype#CLASS_NO_CLASS}</li>
	 * <li>{@link Datatype#CLASS_OPAQUE}</li>
	 * <li>{@link Datatype#CLASS_REFERENCE}</li>
	 * </ul>
	 * 
	 * @param type
	 *            - The data type of the original HDF attribute.
	 * @param name
	 *            - The name of the attribute.
	 * @param order
	 *            - The order of magnitude this attribute has.
	 * @param index
	 *            - The member index of the attribute in the data set and
	 *            subindices if existent.
	 */
	private void addAttribute(Datatype type, String name, int order,
			int... index) {
		int HDFType = type.getDatatypeClass();
		boolean isArray = HDFType == Datatype.CLASS_ARRAY
				|| HDFType == Datatype.CLASS_VLEN;
		boolean isMultiDim = !isComp && order > 1 && index.length == 1;
		if (isArray || isMultiDim) {
			int[] dims;
			if (index.length > 1) {
				// ignore arrays of arrays for now
				// are not supported by HDF5 anyway
				return;
			}
			if (isMultiDim) { // scalar data set with dimensions
				long dsdims[] = dataset.getDims();
				dims = new int[dsdims.length - 1];
				long dim;
				for (int i = 1; i < dsdims.length; i++) {
					dim = dsdims[i];
					if (dim > Integer.MAX_VALUE) {
						// ignore this attribute
						return;
					}
					dims[i - 1] = (int) dim;
				}
			} else { // compound data set array attribute
				dims = ((CompoundDS) dataset).getMemberDims(index[0]);
				type = type.getBasetype();
			}
			String suffix;
			int tmpOrder, d;
			index = Arrays.copyOf(index, index.length + 1);
			for (int i = 0; i < order; i++) {
				index = Arrays.copyOf(index, index.length);
				// subindex for this part of the array attribute
				index[index.length - 1] = i;
				suffix = "";
				tmpOrder = i;
				// build suffix "[i1]...[ik]"
				for (int j = dims.length - 1; j >= 0; j--) {
					d = dims[j];
					suffix = "[" + (tmpOrder % d) + "]" + suffix;
					tmpOrder /= d;
				}
				addAttribute(type, name + suffix, order, index);
			}
			return;
		}
		// map HDF 5 data types to RapidMiner ontology
		int RMType;
		String[] enums = null;
		switch (HDFType) {
		case Datatype.CLASS_INTEGER:
		case Datatype.CLASS_CHAR:
			RMType = Ontology.INTEGER;
			break;
		case Datatype.CLASS_FLOAT:
			RMType = Ontology.REAL;
			break;
		case Datatype.CLASS_ENUM:
			enums = type.getEnumMembers().split(",");
			RMType = enums.length > 2 ? Ontology.POLYNOMINAL
					: Ontology.BINOMINAL;
			break;
		case Datatype.CLASS_STRING:
			enums = createEnums(order, index);
			RMType = enums == null ? Ontology.STRING
					: enums.length > 2 ? Ontology.POLYNOMINAL
							: Ontology.BINOMINAL;
			break;
		case Datatype.CLASS_TIME:
			RMType = Ontology.DATE_TIME;
			break;
		// case Datatype.CLASS_ARRAY: // is dealt with above
		// case Datatype.CLASS_VLEN: // is dealt with above
		// case Datatype.CLASS_BITFIELD:
		// case Datatype.CLASS_COMPOUND:
		// case Datatype.CLASS_NO_CLASS:
		// case Datatype.CLASS_OPAQUE:
		// case Datatype.CLASS_REFERENCE:
		default:
			// TODO? Idea: represent as string
			return;
		}
		// create attribute and mappings
		Attribute att = AttributeFactory.createAttribute(name, RMType);
		List<Integer> RM2HDF = null;
		HDF5Attribute hdf5Att;
		if (enums != null && att instanceof NominalAttribute) {
			RM2HDF = createNominalMapping((NominalAttribute) att, enums);
			hdf5Att = new HDF5Attribute(index, order, RM2HDF);
		} else {
			hdf5Att = new HDF5Attribute(index, order);
		}
		h5AttMapping.put(super.addAttribute(att), hdf5Att);
		if (!usedMembers.contains(index[0])) {
			usedMembers.add(index[0]);
		} else {
			if (arrayMembers == null) {
				arrayMembers = new HashMap<Integer, Integer>();
			}
			int count = 2;
			if (arrayMembers.containsKey(index[0])) {
				count = arrayMembers.get(index[0]) + 1;
			}
			arrayMembers.put(index[0], count);
		}
	}

	/**
	 * Creates enum mappings from a {@link Datatype#CLASS_STRING} attribute of
	 * the {@link Dataset}.
	 * 
	 * @param order
	 *            - The order of the attribute.
	 * @param index
	 *            - The index and subindices of the attribute.
	 * @return the "name=value" pairs of the generated enum mapping.
	 */
	@SuppressWarnings("unchecked")
	private String[] createEnums(int order, int... index) {
		Set<String> strings = new HashSet<String>();
		Object data;
		// select only this member
		if (isComp) {
			((CompoundDS) dataset).selectMember(index[0]);
		}
		windowSize = CACHE_MEMORY;
		int exampleIndex = 0;
		while (exampleIndex < dataset.getDims()[0]) {
			setActiveWindow(exampleIndex);
			try {
				data = dataset.getData();
			} catch (Exception e) {
				return null;
			}
			if (isComp) { // get attribute data
				try {
					data = ((List<Object>) data).get(0);
				} catch (ClassCastException cce) {
					return null;
				}
			}
			// gather strings from sub data to minimize mapping
			if (index.length > 1) {
				Object arr;
				for (int i = 0; i < size(); i++) {
					arr = Array.get(data, order * i + index[1]);
					for (int j = 2; j < index.length; j++) {
						arr = Array.get(arr, index[j]);
					}
					strings.add((String) arr);
				}
			} else { // no array attribute, get all data
				strings.addAll(Arrays.asList((String[]) data));
			}
			dataset.clear();
			exampleIndex += windowSize;
		}
		// deselect member again
		if (isComp) {
			((CompoundDS) dataset).setMemberSelection(false);
		}
		String[] enums = new String[strings.size()];
		int i = 0;
		for (String string : strings) {
			enums[i] = string + "=" + i++;
		}
		return enums;
	}

	/**
	 * Creates the nominal mapping for RapidMiner and returns the mapping
	 * between RapidMiner and HDF indices regarding the given "name=value"
	 * pairs.
	 * 
	 * @param att
	 *            - The nominal attribute to create the mapping for.
	 * @param enums
	 *            - The "name=value" pairs.
	 * @return the mapping between RapidMiner and HDF indices.
	 */
	private static List<Integer> createNominalMapping(NominalAttribute att,
			String[] enums) {
		NominalMapping mapping;
		String[] kv;
		int key;
		String value;
		Map<Integer, String> map = new HashMap<Integer, String>();
		List<Integer> RM2HDF = new ArrayList<Integer>();
		int mapkey = 0;
		for (String e : enums) {
			kv = e.split("=");
			value = kv[0];
			key = new Integer(kv[1]);
			map.put(mapkey++, value);
			RM2HDF.add(key);
		}
		if (enums.length <= 2) {
			mapping = new BinominalMapping();
			for (int i : map.keySet()) {
				mapping.setMapping(map.get(i), i);
			}
		} else {
			mapping = new PolynominalMapping(map);
		}
		att.setMapping(mapping);
		return RM2HDF;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public DataRowReader getDataRowReader() {
		try {
			maxLoadedIndex = -1;
			return new HDF5DatasetDataRowReader(this);
		} catch (OutOfMemoryError e) {
			throw new RuntimeException("Out of memory", e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public DataRow getDataRow(int index) {
		try {
			return getDataRow(index, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// safety
		return getDataRow(index, false);
	}

	/**
	 * Returns the i-th data row like {@link #getDataRow(int)} , but gives the
	 * possibility to preload the window belonging to the specified index. This
	 * will first check if all needed data rows are in the cache.
	 * 
	 * @param index
	 *            - The index of the example.
	 * @param preload
	 *            - If true, the whole window will be in the cache after this
	 *            method returns.
	 * @return - the data row corresponding to the specified index.
	 * @see #getDataRow(int)
	 */
	@SuppressWarnings("unchecked")
	private DataRow getDataRow(int index, boolean preload) {
		if (!preload && rowCache.containsKey(index)) {
			return rowCache.get(index).get();
		}
		setActiveWindow(index);
		// add read lock
		synchronized (locks) {
			locks[0]++;
		}
		int relIndex = index % windowSize;
		synchronized (cancelled) {
			// reading, so start timer
			if (cancelled[0]) {
				clearer = setCacheTimer();
			}
		}
		Object data;
		try {
			data = dataset.getData();
		} catch (Exception e) {
			return null;
		}
		double[][] rowData;
		int numberOfRows = 1;
		if (preload) {
			numberOfRows = (int) (dataset.getSelectedDims()[0] - relIndex);
		}
		int[] indices = new int[getAttributeCount()];
		Map<Integer, Integer> indexMapping = new HashMap<Integer, Integer>();
		int indexOfAtt = 0, attIndex;
		for (Attribute a : getAttributes()) {
			if (a == null) {
				continue;
			}
			attIndex = a.getTableIndex();
			indices[indexOfAtt] = attIndex;
			indexMapping.put(attIndex, indexOfAtt++);
		}
		rowData = new double[numberOfRows][getAttributeCount()];
		HDF5Attribute hdf5Att;
		Object arr, value;
		Number nr;
		int h5Index;
		double[] attValues;
		df = null;
		// get data from HDF backed attributes
		for (Entry<Integer, HDF5Attribute> entry : h5AttMapping.entrySet()) {
			attIndex = entry.getKey();
			indexOfAtt = indexMapping.get(attIndex);
			hdf5Att = entry.getValue();
			h5Index = hdf5Att.getIndex();
			// get new values corresponding to this window
			if (newValues != null && newValues.containsKey(attIndex)) {
				attValues = newValues.get(attIndex)[index / windowSize];
			} else {
				attValues = null;
			}
			// get column array
			if (isComp) {
				arr = ((List<Object>) data).get(usedMembers.indexOf(h5Index));
			} else {
				arr = data;
			}
			for (int i = 0; i < numberOfRows; i++) {
				// check for overwritten values
				if (attValues != null
						&& !Double.isInfinite(attValues[relIndex + i])) {
					rowData[i][indexOfAtt] = attValues[relIndex + i];
					continue;
				}
				if (hdf5Att.isArray()) {
					value = arr;
					try {
						for (int j : hdf5Att.getSubindices(relIndex + i)) {
							value = Array.get(value, j);
						}
					} catch (ArrayIndexOutOfBoundsException aiooe) {
						// variable length arrays?!
						rowData[i][indexOfAtt] = Double.NaN;
						continue;
					}
				} else {
					value = Array.get(arr, relIndex + i);
				}
				if (hdf5Att.isPolynominal()) {
					// enum conversion
					if (value instanceof String) {
						nr = getAttribute(attIndex).getMapping().getIndex(
								(String) value);
					} else {
						nr = hdf5Att.map2RM(((Number) value).intValue());
					}
				} else if (value instanceof String) {
					// date/time conversion
					nr = parseDateTime((String) value);
				} else {
					nr = (Number) value;
				}
				rowData[i][indexOfAtt] = nr.doubleValue();
			}
		}
		// get data from new attributes
		if (newAttributes != null) {
			double[] values;
			for (Entry<Integer, double[][]> entry : newAttributes.entrySet()) {
				indexOfAtt = indexMapping.get(entry.getKey());
				values = entry.getValue()[index / windowSize];
				for (int i = 0; i < numberOfRows; i++) {
					rowData[i][indexOfAtt] = values[relIndex + i];
				}
			}
		}
		// remove read lock
		synchronized (locks) {
			locks[0]--;
			lastAccess = System.currentTimeMillis();
		}
		// create and cache data rows
		DataRow row;
		for (int i = 0; i < numberOfRows; i++) {
			row = new HDF5DatasetDataRow(indices, rowData[i], index + i, this);
			rowCache.put(index + i, new SoftReference<DataRow>(row));
		}
		return getDataRow(index);
	}

	/**
	 * Sets the window to be read for the specified index. Returns wether the
	 * window changed.
	 * 
	 * @param index
	 *            - The index to be made available.
	 * @return true if a new window was loaded.
	 */
	synchronized boolean setActiveWindow(int index) {
		int relIndex = index - currentWindowStart;
		// no change
		if (index < 0 || index >= size || relIndex >= 0
				&& relIndex < dataset.getSelectedDims()[0]) {
			return false;
		}
		// TODO separate Thread, wait for locks to release?
		// also: if so, queue?
		while (locks[0] != 0) {
		}
		clearDataset();
		clearCacheTimer();
		// select new hyperslab
		currentWindowStart = (index / windowSize) * windowSize;
		dataset.getStartDims()[0] = currentWindowStart;
		if (currentWindowStart + windowSize >= size) {
			dataset.getSelectedDims()[0] = size - currentWindowStart;
		} else {
			dataset.getSelectedDims()[0] = windowSize;
		}
		return true;
	}

	/**
	 * Parses the given string as a {@link Date} and returns the time stamp of
	 * that date. This method tries all different time, date and time/date
	 * format combinations to fit the given string. If there is no suitable
	 * parser, the value is treated as missing.
	 * 
	 * @param value
	 *            The date time string representation.
	 * @return the corresponding time stamp.
	 */
	private Number parseDateTime(String value) {
		// try the current date format for this string
		if (df != null) {
			try {
				return df.parse(value).getTime();
			} catch (ParseException e) {
				df = null;
			}
		}
		// try different time styles
		for (int tStyle = 0; tStyle < 4; tStyle++) {
			df = DateFormat.getTimeInstance(tStyle);
			try {
				return df.parse(value).getTime();
			} catch (ParseException e) {
				df = null;
			}
			// try different date/time style combinations
			for (int dStyle = 0; dStyle < 4; dStyle++) {
				df = DateFormat.getDateTimeInstance(dStyle, tStyle);
				try {
					return df.parse(value).getTime();
				} catch (ParseException e) {
					df = null;
				}
			}
		}
		// try different date styles
		for (int dStyle = 0; dStyle < 4; dStyle++) {
			df = DateFormat.getDateInstance(dStyle);
			try {
				return df.parse(value).getTime();
			} catch (ParseException e) {
				df = null;
			}
		}
		return Double.NaN;
	}

	/**
	 * Preloads the next window if a certain point in the current window is
	 * reached, dependent on {@link #PRELOAD_RATIO}.
	 * 
	 * @param index
	 *            - The index in the current window to check against.
	 */
	public void preload(int index) {
		int futureIndex = (int) (index + windowSize * PRELOAD_RATIO);
		// load next "chunk" into cache
		// TODO: separate Thread?
		int futuremaxIndex = currentWindowStart + 2 * windowSize - 1;
		if (futureIndex < size && futuremaxIndex > maxLoadedIndex) {
			getDataRow(futureIndex, true);
			maxLoadedIndex = futuremaxIndex;
		}
	}

	/**
	 * Changes the value given by index (attribute) and row (example).
	 * 
	 * @param row
	 *            - The index of the example.
	 * @param index
	 *            - The index of the attribute.
	 * @param value
	 *            - The new value.
	 * @see #setNewValue(int, int, double)
	 */
	public void set(int row, int index, double value) {
		HDF5Attribute hdf5Att = h5AttMapping.get(index);
		if (hdf5Att != null) {
			setNewValue(row, index, value);
		} else {
			newAttributes.get(index)[row / windowSize][row % windowSize] = value;
		}
	}

	/**
	 * Changes the value of an HDF backed attribute by using a map of new
	 * values.
	 * 
	 * @param row
	 *            - The index of the example.
	 * @param index
	 *            - The index of the attribute.
	 * @param value
	 *            - The new value.
	 * @see #set(int, int, double)
	 */
	private void setNewValue(int row, int index, double value) {
		if (newValues == null) {
			newValues = new HashMap<Integer, double[][]>();
		}
		double[][] attValues = newValues.get(index);
		if (attValues == null) {
			attValues = new double[(int) Math
					.ceil(((double) size) / windowSize)][];
			newValues.put(index, attValues);
		}
		double[] values = attValues[row / windowSize];
		if (values == null) {
			// trim to size if necessary
			if (row / windowSize == attValues.length - 1
					&& attValues.length * windowSize > size) {
				values = new double[size % windowSize];
			} else {
				values = new double[windowSize];
			}
			Arrays.fill(values, Double.POSITIVE_INFINITY);
			attValues[row / windowSize] = values;
		}
		values[row % windowSize] = value;
		checkCompatibility(index, value);
	}

	/**
	 * Checks if the given value is compatible to the HDF5 backed attribute with
	 * the specified index. If it is not compatible, the data type will be
	 * changed when the table is written to file.
	 * 
	 * @param index
	 *            The index of the attribute to check against.
	 * @param value
	 *            The value to check.
	 */
	private void checkCompatibility(int index, double value) {
		// already known
		if (compatible != null && compatible.contains(index)) {
			return;
		}
		// not null, called from setNewValue
		HDF5Attribute h5a = h5AttMapping.get(index);
		Datatype t;
		if (isComp) {
			t = ((CompoundDS) dataset).getMemberTypes()[h5a.getIndex()];
		} else {
			t = dataset.getDatatype();
		}
		if (t != HDF5WriteUtil.checkForUpgrade(t, value, value)) {
			if (compatible == null) {
				compatible = new ArrayList<>();
			}
			compatible.add(index);
		}
	}

	@Override
	public synchronized int addAttribute(Attribute att) {
		int index = super.addAttribute(att);
		if (newAttributes == null) {
			newAttributes = new TreeMap<Integer, double[][]>();
		}
		updateCacheSize();
		double[][] attData = new double[(int) Math.ceil(((double) size)
				/ windowSize)][windowSize];
		// trim last part to size
		if (attData.length * windowSize > size) {
			attData[attData.length - 1] = new double[size % windowSize];
		}
		newAttributes.put(index, attData);
		return index;
	}

	/**
	 * Changes the current cache and window size and reorganizes the data
	 * structure holding new attributes and new values for HDF5 backed
	 * attributes.
	 * 
	 */
	void updateCacheSize() {
		int attCount = getAttributeCount();
		if (attCount != 0) {
			maxCacheSize = CACHE_MEMORY / attCount;
			windowSize = (int) (maxCacheSize * WINDOW_CACHE_RATIO);
			if (windowSize > size) {
				// maximum window size is size of the whole set
				windowSize = size;
			} else if (chunkSize != -1 && windowSize > chunkSize) {
				// if the chunks are smaller than the window, make window size a
				// multiple of chunk size
				windowSize -= windowSize % chunkSize;
			}
			// windowSize = (int) Math
			// .min(maxCacheSize * WINDOW_CACHE_RATIO, size);
			dataset.getSelectedDims()[0] = windowSize;
			currentWindowStart = 0;
			reorganizeStaticData();
		}
	}

	/**
	 * Reorganizes the matrices of the new attributes and new values to
	 * correspond to the current window size.
	 */
	void reorganizeStaticData() {
		reorganizeStaticData(newValues, Double.POSITIVE_INFINITY);
		reorganizeStaticData(newAttributes, null);
	}

	/**
	 * Reorganizes the matrices of the given map with the specified fill value.
	 * If no fill value is given, it is assumed that every cell in the amtrix
	 * will be used.
	 * 
	 * @param map
	 *            The map containing the matrices.
	 * @param fillValue
	 *            The fill value for unused matrix cells or null if all cells
	 *            are to be used.
	 */
	private void reorganizeStaticData(Map<Integer, double[][]> map,
			Double fillValue) {
		double[][] tmp;
		if (map == null) {
			return;
		}
		int windows = (int) Math.ceil(((double) size) / windowSize);
		for (Entry<Integer, double[][]> entry : map.entrySet()) {
			if (fillValue == null) {
				tmp = new double[windows][windowSize];
			} else {
				tmp = new double[windows][];
			}
			ConverterUtil.reorganize(entry.getValue(), tmp, size, fillValue);
			entry.setValue(tmp);
		}
	}

	@Override
	public synchronized void removeAttribute(int index) {
		Attribute old = getAttribute(index);
		if (old == null) {
			return;
		}
		super.removeAttribute(index);
		HDF5Attribute hdf5Att = h5AttMapping.remove(index);
		newAttributes.remove(index);
		updateCache(old);
		updateCacheSize();
		// remove new attribute
		if (hdf5Att == null) {
			if (newAttributes.isEmpty()) {
				newAttributes = null;
			}
			return;// ?
		}
		if (newValues != null && newValues.remove(index) != null
				&& newValues.isEmpty()) {
			newValues = null;
			compatible = null;
		}
		if (compatible != null) {
			compatible.remove(index);
		}
		if (removedAtts == null) {
			removedAtts = new ArrayList<HDF5Attribute>();
		}
		removedAtts.add(hdf5Att);
		int memberIndex = hdf5Att.getIndex();
		// array member handling
		if (hdf5Att.isArray()) {
			int count = arrayMembers.get(memberIndex);
			if (count > 1) {
				// member still needed
				arrayMembers.put(memberIndex, count - 1);
				return;
			} else {
				arrayMembers.remove(memberIndex);
			}
		}
		if (!usedMembers.remove((Integer) memberIndex)) {
			return;
		}
		// reselect members
		if (isComp) {
			CompoundDS comp = (CompoundDS) dataset;
			if (!comp.isMemberSelected(memberIndex)) {
				return;
			}
			synchronized (locks) {
				locks[0]++;
			}
			comp.setMemberSelection(false);
			for (int m : usedMembers) {
				comp.selectMember(m);
			}
			synchronized (locks) {
				locks[0]--;
				comp.clear();
			}
		}
	}

	/**
	 * Updates the cache after the specified attribute has been removed so the
	 * currently used data rows can be adjusted.
	 * 
	 * @param old
	 *            The attribute that has been removed.
	 */
	private void updateCache(Attribute old) {
		double def = old.getDefault();
		for (SoftReference<DataRow> dr : rowCache.values()) {
			if (dr.get() != null) {
				// default value triggers deletion of attribute in data row
				dr.get().set(old, def);
			}
		}
	}

	/**
	 * Sets the flag on this table if it should be materialized in memory on
	 * example set creation.
	 * 
	 * @param inMemory
	 *            - specifies if this table should be converted to an in memory
	 *            table on example set creation.
	 * @return this table.
	 * @see HDF5Reader#doWork()
	 */
	public ExampleTable createInMemory(boolean inMemory) {
		this.inMemory = inMemory;
			return this;
		}

	/**
	 * Creates a {@link SimpleExampleSet} from this
	 * {@link HDF5DatasetExampleTable}. {@inheritDoc}
	 */
	@Override
	public ExampleSet createExampleSet(Map<Attribute, String> specialAttributes) {
		ExampleSet set;
		if (inMemory) {
			set = Compatiblity.getDefault().createInMemory(this);
		} else {
			set = super.createExampleSet(specialAttributes);
		}
		set.getAnnotations().setAnnotation(Annotations.KEY_SOURCE, dataset.getFullName());
		set.getAnnotations().setAnnotation(Annotations.KEY_FILENAME, dataset.getFile());
		return set;
	}

	/**
	 * Starts the timer that checks how much new values and attributes have been
	 * added.
	 * 
	 * @see #checkForTemp()
	 */
	private void startUpdateTimer() {
		updateTimer = new Timer(true);
		updateTimer.schedule(new TimerTask() {

			@Override
			public void run() {
				checkForTemp();
			}
		}, 1000, 1000);
	}

	/**
	 * Checks whether a certain amount of new attributes and or values are
	 * present. If so, the table is completely written to a temporary file to
	 * minimize memory consumption.
	 */
	protected synchronized void checkForTemp() {
		int updateSize = 0;
		if (newValues != null) {
			updateSize += newValues.size();
		}
		if (newAttributes != null) {
			updateSize += newAttributes.size();
		}
		if (updateSize == 0
				|| (updateSize * 2 <= getAttributeCount() && updateSize * size <= CACHE_MEMORY)) {
			// not enough updates
			return;
		}
		if ((compatible == null || compatible.isEmpty())
				&& (newAttributes == null || newAttributes.isEmpty())) {
			// copy to temp file if not already temporary
			// write changed values to file
			copyToTemp();
		} else {
			// create new temp file with new attributes
			makeTemp();
		}
	}

	/**
	 * Copies the current underlying {@link Dataset} to a temporary file if the
	 * underlying data set is not already temporary and then writes changes to
	 * existing attributes to that data set.
	 * 
	 * @return true if the operation was successful
	 */
	@SuppressWarnings("unchecked")
	protected boolean copyToTemp() {
		Dataset copy = dataset;
		// copy to a new tmp file if necessary
		if (!isTemp) {
			H5File h5tmp = getNewTempFile();
			if (h5tmp == null) {
				return false;
			}
			DefaultMutableTreeNode node;
			try {
				node = (DefaultMutableTreeNode) h5tmp.copy(dataset,
						(Group) h5tmp.get("/"), dataset.getName());
			} catch (Exception e) {
				return false;
			}
			copy = (Dataset) node.getUserObject();
			copy.init();
			copy.getStride();
			if (copy instanceof CompoundDS) {
				// set selected members
				((CompoundDS) copy).setMemberSelection(false);
				for (int i = 0; i < ((CompoundDS) copy).getMemberCount(); i++) {
					if (((CompoundDS) dataset).isMemberSelected(i)) {
						((CompoundDS) copy).selectMember(i);
					}
				}
			}
		}
		int mIndex;
		HDF5Attribute h5Att;
		Attribute att;
		double[][] newVal;
		Object data;
		int i;
		// write changed values; not null at this point
		for (Entry<Integer, double[][]> entry : newValues.entrySet()) {
			att = getAttribute(entry.getKey());
			h5Att = h5AttMapping.get(entry.getKey());
			mIndex = h5Att.getIndex();
			mIndex = usedMembers.indexOf(mIndex);
			newVal = entry.getValue();
			i = 0;
			copy.getSelectedDims()[0] = windowSize;
			for (double[] newValWin : newVal) {
				if (newValWin == null) {
					i++;
					continue;
				}
				copy.getStartDims()[0] = i * windowSize;
				if ((i + 1) * windowSize >= size) {
					// select rest of data set
					copy.getSelectedDims()[0] = size - i * windowSize;
				}
				try {
					data = copy.getData();
				} catch (Exception e) {
					return false;
				}
				if (isComp) {
					data = ((List<Object>) data).get(mIndex);
				}
				HDF5WriteUtil.transformNewValues(att, h5Att, newValWin, 0,
						data, 0, (int) copy.getSelectedDims()[0]);
				try {
					copy.write();
				} catch (Exception e) {
					return false;
				}
				copy.clear();
				i++;
			}
		}
		replaceDataset(copy);
		newValues = null;
		isTemp = true;
		return true;
	}

	/**
	 * Creates and returns a new temporary HDF5 file that will be deleted after
	 * RapidMiner is closed. Will return null if the file could not be created
	 * or formatted.
	 * 
	 * @return a new temporary {@link H5File}.
	 */
	private static H5File getNewTempFile() {
		File tmpFile;
		try {
			tmpFile = File.createTempFile("hdftmp", ".hd5");
		} catch (IOException e) {
			return null;
		}
		tmpFile.deleteOnExit();
		FileFormat format = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);
		if (format == null) {
			return null;
		}
		try {
			return (H5File) format.createFile(tmpFile.getAbsolutePath(),
					FileFormat.FILE_CREATE_OPEN);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Replaces the {@link #dataset data set} of this table with the specified
	 * one and resets the table.
	 * 
	 * @param newSet
	 *            The new data set for this table
	 */
	private void replaceDataset(Dataset newSet) {
		clearDataset();
		if (isTemp) {
			// remove old temp file if no longer needed
			try {
				new File(dataset.getFile()).delete();
			} catch (Exception e) {
			}
		}
		dataset = newSet;
		isComp = dataset instanceof CompoundDS;
		locks[0] = 0;
		updateCacheSize();
		maxLoadedIndex = -1;
		rowCache.clear();
		if (clearer == null) {
			clearer = setCacheTimer();
		}
		System.gc();
	}

	/**
	 * Make this table's {@link #dataset data set} temporary, such that the
	 * currently static values hold by ({@link #newValues} and
	 * {@link #newAttributes}) will be written to file, reducing memory usage.
	 * The new {@link Dataset} will have the same parameters as the current one
	 * regarding compression and chunking.
	 * 
	 * @return true if the operation was successful.
	 */
	protected boolean makeTemp() {
		H5File h5tmp = getNewTempFile();
		if (h5tmp == null) {
			return false;
		}
		Dataset tmpDS;
		try {
			// create new data set based on the parameters of the current data
			// set.
			String comp = dataset.getCompression();
			int index = comp.indexOf("level = ");
			int gzip = 6;
			long[] chunks = dataset.getChunkSize();
			if (chunks == null) {
				chunks = new long[] { windowSize };
			}
			if (index >= 0) {
				try {
					gzip = new Integer(comp.substring(index + 8));
				} catch (Exception e) {
				}
			}
			tmpDS = HDF5WriteUtil.createDataset(this, (Group) h5tmp.get("/"),
					dataset.getName(), null, 0, chunks, gzip, size);
		} catch (Exception e) {
			return false;
		}
		if (tmpDS == null) {
			return false;
		}
		tmpDS.init();
		try {
			// initialize meta data
			tmpDS.getMetadata();
		} catch (Exception e) {
		}
		// if writing was not successful, ignore but do not proceed
		if (HDF5WriteUtil.writeToDataset((ExampleTable) this, tmpDS, null) != null) {
			return false;
		}

		// attribute remapping
		SortedMap<Integer, HDF5Attribute> newMap = new TreeMap<>(h5AttMapping);
		HDF5Attribute h5Att;
		Attribute a;
		List<Integer> enumMapping;
		for (int i : newAttributes.keySet()) {
			a = getAttribute(i);
			if (a.isNominal()) {
				enumMapping = new ArrayList<>(a.getMapping().size());
				for (int j = 0; j < enumMapping.size(); j++) {
					enumMapping.add(j);
				}
				h5Att = new HDF5Attribute(null, enumMapping);
			} else {
				h5Att = new HDF5Attribute(null);
			}
			newMap.put(i, h5Att);
		}
		int mIndex = 0;
		for (HDF5Attribute h5a : newMap.values()) {
			h5a.setIndex(new int[] { mIndex++ });
		}
		// replace data set and reset table
		replaceDataset(tmpDS);
		newValues = newAttributes = null;
		usedMembers = new ArrayList<>(getAttributeCount());
		for (int i = 0; i < getAttributeCount(); i++) {
			usedMembers.add(i);
		}
		arrayMembers = null;
		isTemp = true;
		removedAtts = null;
		h5AttMapping = newMap;
		return true;
	}

	/**
	 * Clears the timer task if possible.
	 */
	private void clearCacheTimer() {
		if (clearer != null) {
			try {
				clearer.cancel();
			} catch (Exception e) {
			}
			clearer = null;
			synchronized (cancelled) {
				cancelled[0] = true;
			}
			clearTimer.purge();
		}
	}

	/**
	 * Initializes a {@link TimerTask} that tries to free the memory hold by the
	 * corresponding data set every second.
	 * 
	 * @return the clear timer task.
	 */
	private TimerTask setCacheTimer() {
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				clearDataset();
			}
		};
		clearTimer.schedule(task, 1000, 1000);
		synchronized (cancelled) {
			cancelled[0] = false;
		}
		return task;
	}

	/**
	 * Clears the {@link #dataset data set} if no more I/O access is registered.
	 */
	private void clearDataset() {
		boolean cancel;
		synchronized (locks) {
			// make sure no one reads anymore and some time has passed before
			// clearing
			cancel = dataset != null && locks[0] == 0
					&& System.currentTimeMillis() - 1000 > lastAccess;

			if (cancel) {
				// clear data and remove timer task
				dataset.clear();
				clearCacheTimer();
				System.gc();
			}
		}
	}

	@Override
	protected void finalize() {
		clearCacheTimer();
		clearTimer.cancel();
		// updateTimer.cancel();
	}

	// -------------------
	// unused methods of memory example table
	// -------------------
	/**
	 * Throws {@link UnsupportedOperationException}
	 */
	@Override
	public void readExamples(DataRowReader i, boolean permute, Random random) {
		throw new UnsupportedOperationException(
				"HDF backed example table can not read data rows");
	}

	/**
	 * Throws {@link UnsupportedOperationException}
	 */
	@Override
	public void addDataRow(DataRow dataRow) {
		throw new UnsupportedOperationException(
				"HDF backed example table can not add data rows");
	}

	/**
	 * Throws {@link UnsupportedOperationException}
	 */
	@Override
	public boolean removeDataRow(DataRow dataRow) {
		throw new UnsupportedOperationException(
				"HDF backed example table can not remove data rows");
	}

	/**
	 * Throws {@link UnsupportedOperationException}
	 */
	@Override
	public void clear() {
		throw new UnsupportedOperationException(
				"HDF backed example table can not clear data rows");
	}

}
