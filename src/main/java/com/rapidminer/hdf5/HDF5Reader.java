/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import javax.swing.tree.DefaultMutableTreeNode;

import ncsa.hdf.hdf5lib.exceptions.HDF5Exception;
import ncsa.hdf.object.CompoundDS;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.Group;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5File;

import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.SimpleExampleSet;
import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.RapidMinerGUI;
import com.rapidminer.hdf5.util.Compatiblity;
import com.rapidminer.operator.Annotations;
import com.rapidminer.operator.IOObject;
import com.rapidminer.operator.IOObjectCollection;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.nio.model.AbstractDataResultSetReader;
import com.rapidminer.operator.nio.model.DataResultSetFactory;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.operator.ports.Port;
import com.rapidminer.operator.ports.metadata.CollectionMetaData;
import com.rapidminer.operator.ports.metadata.ExampleSetMetaData;
import com.rapidminer.operator.ports.metadata.MetaData;
import com.rapidminer.parameter.MetaDataProvider;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeAttribute;
import com.rapidminer.parameter.ParameterTypeAttributes;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.UndefinedParameterError;
import com.rapidminer.parameter.conditions.EqualTypeCondition;
import com.rapidminer.tools.Observable;
import com.rapidminer.tools.Observer;

/**
 * This Operator can read data sets from HDF 5 files into example sets.
 *
 * @author Jan Czogalla
 *
 */
public class HDF5Reader extends AbstractDataResultSetReader {

	// parameter keys
	public static final String PARAMETER_HDF_FILE = "hdf5_file";
	public static final String[] HDF5_EXTENSIONS = { "hdf5", "hd5", "h5" };
	public static final String PARAMETER_FILTER_TYPE = "table_filter_type";
	public static final String[] CONDITION_NAMES = new String[] { "single", "subset", "all" };
	public static final String PARAMETER_TABLES_SELECTION = "tables";
	public static final String PARAMETER_TABLE_SELECTION = "table";
	public static final String PARAMETER_INVERT_SELECTION = "invert";
	public static final String PARAMETER_JOIN_ATTRIBUTES = "join_attributes";
	public static final String PARAMETER_MEMORY = "in_memory";

	static {
		// register this reader for Drag&Drop action
		for (String extension : HDF5_EXTENSIONS) {
			registerReaderDescription(new ReaderDescription(extension,
					HDF5Reader.class, PARAMETER_HDF_FILE));
		}
	}

	// fields for meta data creation and preprocessing
	private Vector<String> tableNames, joinAtts;
	private File currentFile;
	private Group currentRoot;
	private H5File currentHDF5File;
	private List<String> tablesSelected;
	private List<String> joinSelected;
	MetaDataProvider mdProvider = new HDF5MetaDataProvider(this);
	private boolean cacheable = false;

	public HDF5Reader(OperatorDescription description) {
		super(description);
		// listen to file input port for connection changes
		getFileInputPort().addObserver(new Observer<Port>() {

			private boolean isConnected = getFileInputPort().isConnected();

			@Override
			public void update(Observable<Port> observable, Port port) {
				boolean connectionChanged = isConnected != port.isConnected();
				if (connectionChanged) {
					isConnected = !isConnected;
				} else {
					return;
				}
				InputPort fip = (InputPort) port;
				if (isConnected) { // newly connected => get (meta) data
					OutputPort fop = fip.getSource();
					IOObject obj = fop.getAnyDataOrNull();
					if (obj != null) {
						fip.receive(obj);
						processFileChange();
					} else {
						MetaData md = null;
						try {
							md = Compatiblity.getDefault().getMetaData(fop, MetaData.class);
							fip.receiveMD(md);
							processFileChange();
						} catch (Exception e) {
							// ignore mismatched MD
						}
					}
				} else { // remove any received data
					fip.receive(null);
					processFileChange();
				}
			}
		}, false);
		// listen to file input port for meta data changes
		getFileInputPort().registerMetaDataChangeListener(
				newMetadata -> processFileChange());
		// listen to parameter changes and update meta data on the fly
		getParameters().addObserverAsFirst((parameters, param) -> {
			if (param == null) {
				return;
			}
			switch (param) {
				case PARAMETER_HDF_FILE:
					processFileChange();
					return;
				case PARAMETER_JOIN_ATTRIBUTES:
					createJoinList();
					return;
				case PARAMETER_FILTER_TYPE:
				case PARAMETER_TABLE_SELECTION:
				case PARAMETER_TABLES_SELECTION:
				case PARAMETER_INVERT_SELECTION:
					try {
						createTableList();
					} catch (UndefinedParameterError e) {
					}
					return;
				default:
			}
		}, false);
	}

	/**
	 * Processes any change made to the file this reader reads from and
	 * resets/recomputes fields and parameter settings.
	 */
	private void processFileChange() {
		File newFile;
		// check for specified file
		if (isFileSpecified()) {
			try {
				newFile = getSelectedFile();
			} catch (Exception e) {
				newFile = null;
			}
		} else {
			newFile = null;
		}
		// if the new file does not fit, reset fields
		if (newFile == null || !newFile.exists() || newFile.isDirectory()) {
			if (currentFile != null) {
				try {
					currentHDF5File.close();
				} catch (HDF5Exception e) {
				}
				cacheable = false;
			}
			currentFile = null;
			currentHDF5File = null;
			currentRoot = null;
			createParameterLists();
			try {
				createTableList();
			} catch (UndefinedParameterError e) {
			}
			System.gc();
			return;
		}
		// no change
		if (newFile.equals(currentFile)) {
			return;
		}
		currentFile = newFile;
		currentHDF5File = getHDF5File(currentFile);
		// problem reading file (may be not an HDF 5 file)
		if (currentHDF5File == null) {
			currentRoot = null;
			createParameterLists();
			try {
				createTableList();
			} catch (UndefinedParameterError e) {
			}
			cacheable = false;
			System.gc();
			return;
		}
		// prepare fields to (re)compute meta data
		try {
			currentHDF5File.open();
		} catch (Exception e1) {
		}
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) currentHDF5File
				.getRootNode();
		currentRoot = (Group) root.getUserObject();
		createParameterLists();
		try {
			createTableList();
		} catch (UndefinedParameterError e) {
		}
		cacheable = false;
		System.gc();
	}

	/**
	 * Get the {@link H5File} from the specified file object. Will return null
	 * if the file could not be opened as an HDF 5 file.
	 *
	 * @param f
	 *            - The file object to open.
	 * @return The HDF 5 file or null if a problem occured.
	 */
	private static H5File getHDF5File(File f) {
		FileFormat format = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);
		if (format == null) {
			return null;
		}
		H5File h5File;
		try {
			h5File = (H5File) format.createFile(f.getAbsolutePath(),
					FileFormat.FILE_CREATE_OPEN);
		} catch (Exception e) {
			return null;
		}
		try {
			h5File.open();
			h5File.close();
		} catch (Exception e) {
			return null;
		}
		return h5File;
	}

	/**
	 * Creates the lists for the {@link HDF5Reader#PARAMETER_TABLE_SELECTION}
	 * and {@link #PARAMETER_JOIN_ATTRIBUTES} parameters. Will extract the
	 * available {@link Dataset}s and common attributes among them.
	 *
	 */
	private void createParameterLists() {
		tableNames.clear();
		joinAtts.clear();
		if (currentHDF5File == null) {
			return;
		}
		List<HObject> members;
		Collection<String> attributes;
		List<Group> groups = new ArrayList<Group>();
		groups.add(currentRoot);
		Group group;
		// collect datasets
		while (!groups.isEmpty()) {
			group = groups.remove(0);
			members = group.getMemberList();
			for (HObject obj : members) {
				if (obj instanceof Group) {
					groups.add((Group) obj);
					continue;
				}
				if (obj instanceof Dataset) {
					tableNames.add(obj.getFullName());
					((Dataset) obj).init();
					if (obj instanceof CompoundDS) {
						attributes = Arrays.asList(((CompoundDS) obj)
								.getMemberNames());
					} else {
						continue;
					}
					if (joinAtts.isEmpty()) {
						joinAtts.addAll(attributes);
					} else {
						joinAtts.retainAll(attributes);
					}
				}
			}
		}
		// refresh parameter view
		MainFrame mf = RapidMinerGUI.getMainFrame();
		if (mf != null) {
			mf.getPropertyPanel().invalidate();
		}
	}

	/**
	 * Creates the list of table names that are currently selected.
	 *
	 * @throws UndefinedParameterError
	 *             if a parameter is not defined.
	 */
	private void createTableList() throws UndefinedParameterError {
		tablesSelected = new ArrayList<String>();
		cacheable = false;
		List<String> subset = new ArrayList<String>();
		int filterType = getParameterAsInt(PARAMETER_FILTER_TYPE);
		boolean invert = false, all = false;

		switch (filterType) {
		case 0:// single table
			tablesSelected.add(getParameter(PARAMETER_TABLE_SELECTION));
			return;
		case 1: // subset of tables
			invert = getParameterAsBoolean(PARAMETER_INVERT_SELECTION);
			subset.addAll(Arrays.asList(getParameterAsString(
					PARAMETER_TABLES_SELECTION).split("\\|")));
			subset.remove("");
			break;
		case 2: // all tables
			all = true;
			break;
		}

		// not all parameters are set
		if (currentRoot == null || tableNames == null || tableNames.isEmpty()) {
			if (!all && !invert) {
				tablesSelected.addAll(subset);
			}
			return;
		}

		if (all || invert) {
			tablesSelected.addAll(tableNames);
		}
		if (invert) {
			tablesSelected.removeAll(subset);
		} else {
			tablesSelected.addAll(subset);
		}
	}

	/**
	 * Creates the list of all attributes that shall not be renamed in
	 * RapidMiner to allow joining several {@link Dataset}s.
	 */
	private void createJoinList() {
		String joins;
		try {
			joins = getParameterAsString(PARAMETER_JOIN_ATTRIBUTES);
		} catch (UndefinedParameterError e) {
			joinSelected = null;
			return;
		}
		joinSelected = new ArrayList<String>(Arrays.asList(joins.split("\\|")));
		joinSelected.remove("");
	}

	@Override
	protected boolean isMetaDataCacheable() {
		return cacheable;
	}

	@SuppressWarnings("deprecation")
	@Override
	public MetaData getGeneratedMetaData() throws OperatorException {
		MetaData md = new ExampleSetMetaData();
		int filterType = getParameterAsInt(PARAMETER_FILTER_TYPE);
		if (tablesSelected != null && !tablesSelected.isEmpty()
				&& currentHDF5File != null) {
			Dataset ds = null;
			String tableName = tablesSelected.get(0);
			try {
				ds = (Dataset) currentHDF5File.get(tableName);
			} catch (Exception e) {
			}
			// if a file is selected and the dataset exists, use meta data from
			// this dataset
			if (ds != null) {
				ExampleSet es = new SimpleExampleSet(
						new HDF5DatasetExampleTable(ds,
								joinSelected != null ? joinSelected
										: new ArrayList<String>()));
				if (currentFile != null) {
					es.getAnnotations().setAnnotation(Annotations.KEY_SOURCE,
							tableName);
					es.getAnnotations().setAnnotation(Annotations.KEY_FILENAME,
							currentFile.getAbsolutePath());
				}
				md = new ExampleSetMetaData(es, true);
			}
		}
		// wrap meta data with collection meta data if necessary
		if (filterType > 0
				&& (tablesSelected == null || tablesSelected.size() != 1)) {
			md = new CollectionMetaData(md);
			if (currentFile != null) {
				md.getAnnotations().setAnnotation(Annotations.KEY_FILENAME,
						currentFile.getAbsolutePath());
			}
		}
		cacheable = true;
		return md;
	}

	@Override
	public void doWork() throws OperatorException {
		if (checkProperties() > 0) {
			throw new OperatorException("Not all mandatory parameters are set.");
		}
		// make sure the fields are properly set
		processFileChange();
		if (currentHDF5File == null) {
			throw new OperatorException("Could not read file "
					+ currentFile.getName() + " as HDF5.");
		}
		if (tablesSelected == null || tablesSelected.isEmpty()) {
			throw new OperatorException("No tables were selected to read!");
		}
		if (joinSelected == null) {
			createJoinList();
		}

		boolean inMem = getParameterAsBoolean(PARAMETER_MEMORY);
		Dataset dataSet;
		ExampleSet exampleSet;
		List<ExampleSet> exampleSets = new ArrayList<ExampleSet>();
		// iterate over selected table names and create example sets
		for (String tableName : tablesSelected) {
			try {
				dataSet = (Dataset) currentHDF5File.get(tableName);
				if (dataSet == null) {
					// null data set causes HDF5DatasetExampleTable to throw an
					// exception
					throw new NullPointerException("Data set must not be null");
				}
			} catch (Exception e) {
				throw new OperatorException("Dataset " + tableName
						+ " does not exist or could not be read from file "
						+ currentFile.getAbsolutePath(), e);
			}
			exampleSet = new HDF5DatasetExampleTable(dataSet, joinSelected)
					.createInMemory(inMem).createExampleSet();
			exampleSets.add(exampleSet);
		}

		IOObject result;
		if (exampleSets.size() == 1) {
			result = exampleSets.get(0);
		} else {
			Collections.sort(exampleSets, (o1, o2) -> -o1.size() * o1.getAttributes().allSize()
					+ o2.size() * o2.getAttributes().allSize());
			result = new IOObjectCollection<ExampleSet>(exampleSets);
			result.getAnnotations().setAnnotation(Annotations.KEY_SOURCE,
					currentFile.getAbsolutePath());
		}

		OutputPort output = getOutputPorts().getPortByName("output");
		output.deliver(result);
	}

	@Override
	protected DataResultSetFactory getDataResultSetFactory()
			throws OperatorException {
		// don't care
		return null;
	}

	@Override
	protected NumberFormat getNumberFormat() throws OperatorException {
		// don't care
		return null;
	}

	@Override
	protected String getFileParameterName() {
		return PARAMETER_HDF_FILE;
	}

	@Override
	protected String getFileExtension() {
		return HDF5_EXTENSIONS[0];
	}

	@Override
	protected String[] getFileExtensions() {
		return HDF5_EXTENSIONS;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<ParameterType> getParameterTypes() {
		if (tableNames == null) {
			tableNames = new Vector<String>();
		}
		if (joinAtts == null) {
			joinAtts = new Vector<String>();
		}
		if (mdProvider == null) {
			mdProvider = new HDF5MetaDataProvider(this);
		}

		List<ParameterType> types = new ArrayList<ParameterType>();
		ParameterType type;

		// file selector
		types.add(makeFileParameterType());

		// select table filter drop down
		types.add(new ParameterTypeCategory(
				PARAMETER_FILTER_TYPE,
				"The condition specifies which tables are selected or affected by this operator.",
				CONDITION_NAMES, 0, false));

		// single table dropdown
		type = new ParameterTypeAttribute(PARAMETER_TABLE_SELECTION,
				"The table that should be read", mdProvider, true) {
			private static final long serialVersionUID = -550879138088370843L;

			@Override
			public Vector<String> getAttributeNames() {
				return tableNames;
			}
		};
		type.setExpert(false);
		type.registerDependencyCondition(new EqualTypeCondition(this,
				PARAMETER_FILTER_TYPE, CONDITION_NAMES, true, 0));
		types.add(type);

		// multi table select dialog
		type = new ParameterTypeAttributes(PARAMETER_TABLES_SELECTION,
				"The list of tables that should be read", null, true) {

			private static final long serialVersionUID = 6960672500033948907L;

			@Override
			public Vector<String> getAttributeNames() {
				return tableNames;
			}
		};
		type.setExpert(false);
		type.registerDependencyCondition(new EqualTypeCondition(this,
				PARAMETER_FILTER_TYPE, CONDITION_NAMES, false, 1));
		types.add(type);

		// multi table invert selection
		type = new ParameterTypeBoolean(PARAMETER_INVERT_SELECTION,
				"Select if all the tables except the selected should be read.",
				false, false);
		type.registerDependencyCondition(new EqualTypeCondition(this,
				PARAMETER_FILTER_TYPE, CONDITION_NAMES, false, 1));
		types.add(type);

		// join attributes selection
		type = new ParameterTypeAttributes(
				PARAMETER_JOIN_ATTRIBUTES,
				"Select which attributes should not be renamed to make a join possible.",
				null, true) {

			private static final long serialVersionUID = 3699392159011210844L;

			@Override
			public Vector<String> getAttributeNames() {
				return joinAtts;
			}
		};
		type.setExpert(false);
		types.add(type);

		// full memory vs on demand (hdf)
		types.add(new ParameterTypeBoolean(
				PARAMETER_MEMORY,
				"Select if the selected table(s) should be completely loaded to memory. This will detach the table(s) from the hdf file.",
				false));

		return types;
	}

	@Override
	protected void finalize() throws Throwable {
		if (currentHDF5File != null) {
			currentHDF5File.close();
		}
		super.finalize();
	}
}
