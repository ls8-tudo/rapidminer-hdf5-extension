/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import static com.rapidminer.hdf5.HDF5Reader.HDF5_EXTENSIONS;
import static com.rapidminer.hdf5.HDF5Reader.PARAMETER_HDF_FILE;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.FileFormat;
import ncsa.hdf.object.Group;
import ncsa.hdf.object.HObject;
import ncsa.hdf.object.h5.H5File;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.AttributeRole;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.ExampleTable;
import com.rapidminer.gui.MainFrame;
import com.rapidminer.gui.RapidMinerGUI;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.io.AbstractExampleSetWriter;
import com.rapidminer.operator.nio.file.FileObject;
import com.rapidminer.operator.nio.file.FileOutputPortHandler;
import com.rapidminer.operator.nio.file.SimpleFileObject;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.operator.ports.metadata.MetaData;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeAttribute;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeFile;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.ParameterTypeLong;
import com.rapidminer.parameter.ParameterTypeString;
import com.rapidminer.tools.LogService;

public class HDF5Writer extends AbstractExampleSetWriter {

	// parameter keys
	public static final String PARAMETER_HDF5_GROUP = "hdf5_group";
	public static final String PARAMETER_TABLE_NAME = "table_name";
	public static final String PARAMETER_APPEND_TABLE = "append_to_table";
	public static final String PARAMETER_REPLACE_TABLE = "replace table";
	public static final String PARAMETER_ENUM_HANDLING = "enum_handling";
	public static final String[] ENUM_HANDLING_OPTIONS = new String[] {
			"dynamic", "only enum", "only string" };
	public static final String PARAMETER_CHUNK_SIZE = "chunk size";
	public static final String PARAMETER_GZIP = "gzip";

	// protected OutputPort exampleOutputPort = getOutputPorts().createPort(
	// "example set");
	protected OutputPort fileOutputPort = getOutputPorts().createPort("file");
	protected FileOutputPortHandler filePortHandler = new FileOutputPortHandler(
			this, fileOutputPort, getFileParameterName());

	private Vector<String> groupNames = new Vector<>();

	public HDF5Writer(OperatorDescription description) {
		super(description);
		// getTransformer().addRule(
		// new PassThroughRule(getInputPorts().getPortByName("input"),
		// exampleOutputPort, false));
		getTransformer().addGenerationRule(fileOutputPort, FileObject.class);
		getParameters().addObserverAsFirst((parameters, parameter) -> {
			if (PARAMETER_HDF_FILE.equals(parameter)) {
				try {
					updateGroupList();
				} catch (Exception e) {
					// ignore
				}
			}
		}, false);
	}

	/**
	 * Updates the list of groups based on the selected HDF5 file.
	 * 
	 * @throws Exception
	 */
	private void updateGroupList() throws Exception {
		File f = getParameterAsFile(PARAMETER_HDF_FILE);
		groupNames.clear();
		// file not set or not existing
		if (f == null || !f.exists()) {
			refreshPropertyPanel();
			return;
		}
		String fileName = f.getAbsolutePath();
		H5File h5File = getHDFFile(fileName);
		Group g = (Group) h5File.get("/");
		// HDF5 file has no root
		// (should not happen)
		if (g == null) {
			refreshPropertyPanel();
			return;
		}
		List<Group> groups = new ArrayList<>();
		groups.add(g);
		while (!groups.isEmpty()) {
			g = groups.remove(0);
			groupNames.add(g.getFullName());
			for (HObject obj : g.getMemberList()) {
				if (obj instanceof Group) {
					groups.add((Group) obj);
				}
			}
		}
		Collections.sort(groupNames);
		refreshPropertyPanel();
	}

	/**
	 * Refreshes the property panel if RapidMiner is not in headless mode.
	 */
	private static void refreshPropertyPanel() {
		MainFrame mf = RapidMinerGUI.getMainFrame();
		if (mf != null) {
			mf.getPropertyPanel().invalidate();
		}
	}

	// TODO parameters as class or object members?
	// RM usually uses local variables, but it would reduce parameters for
	// functions
	@Override
	public ExampleSet write(ExampleSet exampleSet) throws OperatorException {
		String fileName = getParameterAsString(PARAMETER_HDF_FILE);
		String groupName = getParameterAsString(PARAMETER_HDF5_GROUP);
		String tableName = getParameterAsString(PARAMETER_TABLE_NAME);
		// boolean append = getParameterAsBoolean(PARAMETER_APPEND_TABLE);
		boolean replace = getParameterAsBoolean(PARAMETER_REPLACE_TABLE);
		String enumHandling = getParameterAsString(PARAMETER_ENUM_HANDLING);
		int eH;
		for (eH = 0; eH < ENUM_HANDLING_OPTIONS.length; eH++) {
			if (ENUM_HANDLING_OPTIONS[eH].equals(enumHandling)) {
				break;
			}
		}
		long[] chunks = new long[] { getParameterAsLong(PARAMETER_CHUNK_SIZE) };
		int gzip = getParameterAsInt(PARAMETER_GZIP);

		H5File h5File = getHDFFile(fileName);
		Group group = getGroup(h5File, groupName);
		ExampleTable et = exampleSet.getExampleTable();
		if (chunks[0] == 0) {
			// chunking based on existing chunking
			if (et instanceof HDF5DatasetExampleTable) {
				chunks = ((HDF5DatasetExampleTable) et).dataset.getChunkSize();
			} else {
				chunks = null;
			}
		} else if (chunks[0] > exampleSet.size()) {
			throw new OperatorException(
					"Chunk size can not be bigger than example set size.");
		}
		// no chunks => no compression
		if (chunks == null) {
			gzip = 0;
		}
		// if (append) {
		// append = matchAttributes(h5File, group, tableName, exampleSet);
		// }
		HObject old = null;
		try {
			old = h5File.get(group.getFullName() + HObject.separator + tableName);
		} catch (Exception e) {
		}
		Dataset target = getTargetDataset(exampleSet, eH, h5File, group,
				tableName, chunks, gzip, old);
		target.init();
		Exception writeEx = HDF5WriteUtil.writeToDataset(exampleSet, target);
		// if writing fails, inform user
		if (writeEx != null) {
			throw new OperatorException("Could not write example set to file.",
					writeEx);
		}
		// replace existing data set
		if (replace && old != null) {
			boolean changed = true;
			// delete old
			try {
				h5File.delete(old);
			} catch (Exception e) {
				// could not replace => log message with new data set name
				changed = false;
				LogService.getRoot().warning(
						"Could not delete and thus replace old data set "
								+ old.getName() + "\nNew data set is saved as "
								+ target.getName() + "\n" + e.toString());
			}
			if (changed) {
				// rename new set to old name
				try {
					H5File.renameObject(target, tableName);
				} catch (Exception e) {
					changed = false;
					LogService.getRoot().warning(
							"Could not rename new data set to match specified name. It is now saved as "
									+ target.getName() + "\n" + e.toString());
				}
			}
			if (changed) {
				Exception e = new NullPointerException();
				// get the renamed data set, old I/O connections are destroyed
				try {
					target = (Dataset) h5File.get(group.getName() + HObject.separator + tableName);
				} catch (Exception ex) {
					target = null;
					e = ex;
				}
				if (target == null) {
					throw new OperatorException(
							"Could not load renamed data set " + tableName, e);
				}
			}
		}
		// create example table from data set
		HDF5DatasetExampleTable h5Table = new HDF5DatasetExampleTable(target);
		renameAttributes(exampleSet, h5Table);
		fileOutputPort.deliver(new SimpleFileObject(h5File));
		ExampleSet newSet = h5Table.createExampleSet();
		copyRoles(exampleSet, newSet);
		return newSet;
	}

	/**
	 * Fetches the {@link H5File} specified by the file name. Throws exceptions
	 * if the file could not be read, created or is not if the right format.
	 * 
	 * @param fileName
	 *            The file name of the HDF5 file.
	 * @return the HDF5 file associated with the file name.
	 * @throws OperatorException
	 *             If the file could not be read, created or is of the wrong
	 *             format.
	 */
	private static H5File getHDFFile(String fileName) throws OperatorException {
		FileFormat format = FileFormat.getFileFormat(FileFormat.FILE_TYPE_HDF5);
		if (format == null) {
			throw new OperatorException("HDF 5 file format was not found.");
		}
		File f = new File(fileName);
		f = f.getAbsoluteFile();
		createParentFolder(f);
		H5File file;
		try {
			file = (H5File) format.createFile(fileName,
					FileFormat.FILE_CREATE_OPEN);
		} catch (Exception e) {
			throw new OperatorException(
					"File is not an HDF 5 file or could not be created.", e);
		}
		return file;
	}

	/**
	 * Creates the parent folder(s) for the specified file. Throws an exception
	 * if the operation fails.
	 * 
	 * @param f
	 *            The file whose parents should be created.
	 * @throws OperatorException
	 *             If the parent folder(s) could not be created.
	 */
	private static void createParentFolder(File f) throws OperatorException {
		if (f.exists()) {
			return;
		}
		f = f.getParentFile();
		if (!f.exists() && !f.mkdirs()) {
			throw new OperatorException("Could not create parent folder " + f);
		}
	}

	/**
	 * Retrieves or creates a specified group from or in a given {@link H5File}.
	 * Throws an exception if the group could not be found or created.
	 * 
	 * @param h5File
	 *            The HDF5 file to retrieve the group from.
	 * @param groupName
	 *            The name of the group to retrieve.
	 * @return the group with the given name from the specified file.
	 * @throws OperatorException
	 *             If the group could not be retrieved.
	 */
	private static Group getGroup(H5File h5File, String groupName)
			throws OperatorException {
		HObject obj;
		groupName = groupName.trim();
		// if group is empty, use root
		if (groupName.isEmpty()) {
			groupName = HObject.separator;
		}
		// try if the group already exists
		try {
			obj = h5File.get(groupName);
		} catch (Exception e) {
			throw new OperatorException(
					"Could not retrieve group " + groupName, e);
		}
		if (obj != null) {
			return (Group) obj;
		}
		Group g = null;
		// strip away a trailing group separator
		if (groupName.startsWith(HObject.separator)) {
			groupName = groupName.substring(1);
		}
		// create group and parent groups
		// parent group==null => use root
		String[] groupNames = groupName.split(HObject.separator);
		StringBuilder groupNameBuilder = new StringBuilder();
		for (String gName : groupNames) {
			groupNameBuilder.append(HObject.separator).append(gName);
			groupName = groupNameBuilder.toString();
			try {
				g = (Group) h5File.get(groupName);
			} catch (Exception e) {
			}
			if (g == null) {
				try {
					g = h5File.createGroup(groupName, null);
				} catch (Exception e) {
					throw new OperatorException("Could not create group "
							+ groupName, e);
				}
			}
			if (g == null) {
				throw new OperatorException("Could not create group "
						+ groupName, new NullPointerException());
			}
		}
		return g;
	}

	// private boolean matchAttributes(H5File h5File, Group group,
	// String tableName, ExampleSet exampleSet) throws OperatorException {
	// Dataset ds;
	// try {
	// ds = (Dataset) h5File.get(group.getFullName() + Group.separator
	// + tableName);
	// } catch (Exception e) {
	// throw new OperatorException("Could not retrieve data set "
	// + tableName + " from group " + group, e);
	// }
	// if (ds == null) {
	// // ignore append
	// return false;
	// }
	//
	// return true;
	// }

	/**
	 * Determines the target data set to write to. The name for the new data set
	 * is based on the specified name and if it already exists. A new data set
	 * with the calculated name is created in the given {@link H5File} with the
	 * attributes from the example set that is to be written.
	 * 
	 * @param exampleSet
	 *            The example set to write.
	 * @param eH
	 *            The flag indicating enum handling.
	 * @param h5File
	 *            The HDF5 file that should hold the new data set.
	 * @param group
	 *            The group that should hold the new data set.
	 * @param tableName
	 *            The (base) name for the new data set.
	 * @param chunks
	 *            The chunk size for the new data set.
	 * @param gzip
	 *            The compression indicator for the new data set.
	 * @param old
	 *            The already existing data set with the specified name, or null
	 *            if not yet existing.
	 * @return the target data set tow rite to.
	 * @throws OperatorException
	 *             If the data set could not be created.
	 * @see HDF5WriteUtil#createDataset(ExampleSet, int, Group, String, long[],
	 *      int)
	 */
	private static Dataset getTargetDataset(ExampleSet exampleSet, int eH, H5File h5File, Group group, String tableName,
			long[] chunks, int gzip, HObject old) throws OperatorException {
		int copy = 0;
		// calculate final name; same naming rules as HDF5 duplicates
		// (name~copy#)
		if (old != null) {
			tableName += "~copy";
			do {
				try {
					old = h5File.get(group.getFullName() + HObject.separator + tableName + (copy > 0 ? "" + copy : ""));
				} catch (Exception e) {
					old = null;
				}
				copy++;
			} while (old != null);
			copy--;
		}
		Dataset ds = HDF5WriteUtil.createDataset(exampleSet, eH, group,
				tableName + (copy > 0 ? "" + copy : ""), chunks, gzip);
		if (ds == null) {
			throw new OperatorException("Could not create data set "
					+ tableName + " in group " + group);
		}
		return ds;
	}

	/**
	 * Renames attributes in the new example table according to the old example
	 * set, removing unnecessary and/or duplicate prefixes.
	 * 
	 * @param exampleSet
	 *            The old example set that was written.
	 * @param h5Table
	 *            The new example table that was written to.
	 */
	private static void renameAttributes(ExampleSet exampleSet,
			HDF5DatasetExampleTable h5Table) {
		String name;
		String dsName = h5Table.dataset.getFullName().replaceAll(HObject.separator, ".").substring(1);
		Attributes oldAtts = exampleSet.getAttributes();
		for (Attribute a : h5Table.getAttributes()) {
			name = a.getName();
			// check for prefix
			if (oldAtts.get(name) == null && name.startsWith(dsName)) {
				// remove prefix
				name = name.substring(dsName.length() + 1);
				if (oldAtts.get(name) != null) {
					a.setName(name);
				}
			}
		}
	}

	/**
	 * Sets the roles of the new example set based on the old example set.
	 * 
	 * @param exampleSet
	 *            The example set to copy the roles from.
	 * @param newSet
	 *            The new example set to copy the roles to.
	 */
	private static void copyRoles(ExampleSet exampleSet, ExampleSet newSet) {
		Iterator<AttributeRole> iter = exampleSet.getAttributes().allAttributeRoles();
		AttributeRole r;
		Attribute a;
		while (iter.hasNext()) {
			r = iter.next();
			if (!r.isSpecial()) {
				continue;
			}
			a = newSet.getAttributes().get(r.getAttribute().getName());
			if (a != null) {
				newSet.getAttributes().setSpecialAttribute(a,
						r.getSpecialName());
			}
		}
	}

	/**
	 * Returns the name of the {@link ParameterTypeFile} to be added through
	 * which the user can specify the file name.
	 */
	protected static String getFileParameterName() {
		return PARAMETER_HDF_FILE;
	}

	/** Returns the allowed file extensions. */
	protected static String[] getFileExtensions() {
		return HDF5_EXTENSIONS;
	}

	/**
	 * Creates (but does not add) the file parameter named by
	 * {@link #getFileParameterName()} that depends on whether or not
	 * {@link #fileOutputPort} is connected.
	 */
	protected ParameterType makeFileParameterType() {
		return FileOutputPortHandler.makeFileParameterType(this,
				getFileParameterName(), () -> fileOutputPort, getFileExtensions());
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<ParameterType> getParameterTypes() {
		List<ParameterType> types = new LinkedList<ParameterType>();
		types.add(makeFileParameterType());
		ParameterType type;
		type = new ParameterTypeAttribute(PARAMETER_HDF5_GROUP,
				"the group in which the example set should be stored, default (emtpy) is the root group of the file	",
				new HDF5MetaDataProvider(null) {
					@Override
					public MetaData getMetaData() {
						return new MetaData();
					}
				}, true) {

			private static final long serialVersionUID = -4549001763658591124L;

			@Override
			public Vector<String> getAttributeNames() {
				return groupNames;
			}
		};
		types.add(type);
		types.add(new ParameterTypeString(PARAMETER_TABLE_NAME,
				"the name of the table ", false, false));
		// type = new ParameterTypeBoolean(PARAMETER_APPEND_TABLE,
		// "whether or not to append to the specified table", false, false);
		// types.add(type);
		type = new ParameterTypeBoolean(PARAMETER_REPLACE_TABLE,
				"wether or not to replace a table if the name already exists",
				false, false);
		// type.registerDependencyCondition(new BooleanParameterCondition(this,
		// PARAMETER_APPEND_TABLE, false, true));
		types.add(type);
		type = new ParameterTypeCategory(PARAMETER_ENUM_HANDLING,
				"how to write nominal attributes", ENUM_HANDLING_OPTIONS, 0,
				true);
		types.add(type);
		type = new ParameterTypeLong(
				PARAMETER_CHUNK_SIZE,
				"the chunk size for the data set; 0 for no chunking/adopt from existing data set",
				0, Long.MAX_VALUE, 0, true);
		types.add(type);
		type = new ParameterTypeInt(PARAMETER_GZIP,
				"the GZIP compression level", 0, 9, 6, true);
		types.add(type);
		types.addAll(super.getParameterTypes());
		return types;
	}
}