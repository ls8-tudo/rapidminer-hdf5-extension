/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import ncsa.hdf.hdf5lib.exceptions.HDF5ObjectHeaderException;
import ncsa.hdf.object.CompoundDS;
import ncsa.hdf.object.Dataset;
import ncsa.hdf.object.Datatype;
import ncsa.hdf.object.Group;
import ncsa.hdf.object.ScalarDS;
import ncsa.hdf.object.h5.H5CompoundDS;
import ncsa.hdf.object.h5.H5Datatype;
import ncsa.hdf.object.h5.H5ScalarDS;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.Statistics;
import com.rapidminer.example.set.SimpleExampleSet;
import com.rapidminer.example.table.DataRow;
import com.rapidminer.example.table.DataRowReader;
import com.rapidminer.example.table.ExampleTable;
import com.rapidminer.example.table.NominalMapping;
import com.rapidminer.hdf5.util.ConverterUtil;
import com.rapidminer.tools.LogService;
import com.rapidminer.tools.Ontology;

/**
 * A class containing methods to aid writing RapidMiner data to HDF5 files.
 *
 * @author Jan Czogalla
 *
 */
public class HDF5WriteUtil {

	/**
	 * An attribute {@link Comparator}, ordering attributes according to their
	 * table index.
	 *
	 * @see Attribute#getTableIndex()
	 */
	public static final Comparator<Attribute> attributeComparator = (att1, att2) -> {
		if (att1 == null) {
			return att2 == null ? 0 : -1;
		}
		if (att2 == null) {
			return 1;
		}
		return att1.getTableIndex() - att2.getTableIndex();
	};

	/**
	 * Creates a new {@link Dataset} from the given {@link ExampleSet} with all
	 * attributes in the specified {@link Group}.
	 *
	 * @param srcSet
	 *            The source example set.
	 * @param eH
	 *            The flag indicating enum handling.
	 * @param group
	 *            The group to create the new data set in.
	 * @param name
	 *            The name of the new data set.
	 * @param chunks
	 *            The chunking of the new data set.
	 * @param gzip
	 *            The compression level of the new data set.
	 * @return the new data set.
	 * @see #createDataset(ExampleTable, Group, String, List, int, long[], int,
	 *      int)
	 * @see #createCompound(ExampleTable, Group, String, List, int, long[], int,
	 *      int)
	 */
	public static Dataset createDataset(ExampleSet srcSet, int eH, Group group,
			String name, long[] chunks, int gzip) {
		List<Attribute> attributes = new ArrayList<>();
		// example set => all attributes
		Iterator<Attribute> attIter = srcSet.getAttributes().allAttributes();
		while (attIter.hasNext()) {
			attributes.add(attIter.next());
		}
		Collections.sort(attributes, attributeComparator);
		return createDataset(srcSet.getExampleTable(), group, name, attributes,
				eH, chunks, gzip, srcSet.size());
	}

	/**
	 * Creates a new {@link Dataset} from the given {@link ExampleTable} with
	 * the selected attributes in the specified {@link Group}.
	 *
	 * @param srcTable
	 *            The source example table.
	 * @param group
	 *            The group to create the new data set in.
	 * @param name
	 *            The name of the new data set.
	 * @param attributes
	 *            The list of selected attributes.
	 * @param eH
	 *            The flag indicating enum handling.
	 * @param chunks
	 *            The chunking of the new data set.
	 * @param gzip
	 *            The compression level of the new data set.
	 * @param size
	 *            The size of the new data set.
	 * @return the new data set.
	 * @see #createDataset(ExampleSet, int, Group, String, long[], int)
	 * @see #createCompound(ExampleTable, Group, String, List, int, long[], int,
	 *      int)
	 */
	public static Dataset createDataset(ExampleTable srcTable, Group group,
			String name, List<Attribute> attributes, int eH, long[] chunks,
			int gzip, int size) {
		if (group == null || name == null || name.trim().isEmpty() || size < 1) {
			throw new IllegalArgumentException();
		}
		// no attributes selected => select all
		if (attributes == null) {
			attributes = Arrays.asList(srcTable.getAttributes());
		}
		attributes = new ArrayList<>(attributes);
		while (attributes.contains(null)) {
			attributes.remove(null);
		}
		if (attributes.isEmpty()) {
			throw new IllegalArgumentException();
		}
		Collections.sort(attributes, attributeComparator);
		// TODO: creating scalar from compound does not seem to work
		// (32 uInt represented as long)
		// return attributes.size() > 1 ? createCompound(srcTable, group, name,
		// attributes, chunks, gzip, size) : createScalar(srcTable, group,
		// name, attributes, chunks, gzip, size);
		return createCompound(srcTable, group, name, attributes, eH, chunks,
				gzip, size);
	}

	/**
	 * Creates a new {@link CompoundDS} from the given {@link ExampleTable} with
	 * the selected attributes in the specified {@link Group}.
	 *
	 * @param srcTable
	 *            The source example table.
	 * @param group
	 *            The group to create the new data set in.
	 * @param name
	 *            The name of the new data set.
	 * @param attributes
	 *            The list of selected attributes.
	 * @param eH
	 *            The flag indicating enum handling.
	 * @param chunks
	 *            The chunking of the new data set.
	 * @param gzip
	 *            The compression level of the new data set.
	 * @param size
	 *            The size of the new data set.
	 * @return the new compound data set.
	 * @see #createDataset(ExampleSet, int, Group, String, long[], int)
	 * @see #createDataset(ExampleTable, Group, String, List, int, long[], int,
	 *      int)
	 *
	 */
	private static Dataset createCompound(ExampleTable srcTable, Group group,
			String name, List<Attribute> attributes, int eH, long[] chunks,
			int gzip, int size) {
		long[] dims = new long[] { size };
		String[] memberNames = new String[attributes.size()];
		final Datatype[] memberDatatypes = new Datatype[attributes.size()];
		List<Integer> enumIndices = new ArrayList<>();
		// keep at 0/null, no array data types
		int[] memberRanks = new int[attributes.size()];
		long[][] memberDims = new long[attributes.size()][];
		boolean isHDFBacked = srcTable instanceof HDF5DatasetExampleTable;
		HDF5DatasetExampleTable h5Table = null;
		if (isHDFBacked) {
			h5Table = (HDF5DatasetExampleTable) srcTable;
		}
		int i = 0;
		String prefix = name + ".";
		int prefixIndex;
		// create attributes
		for (Attribute a : attributes) {
			memberNames[i] = a.getName();
			prefixIndex = memberNames[i].indexOf(prefix);
			// remove prefixes
			if (prefixIndex >= 0) {
				memberNames[i] = memberNames[i].substring(prefixIndex
						+ prefix.length());
			}
			if (isHDFBacked) {
				memberDatatypes[i] = getDatatypeFromAttribute(a, eH, h5Table,
						size);
			} else {
				memberDatatypes[i] = getDatatypeFromAttribute(a, eH, size);
			}
			if (memberDatatypes[i].getDatatypeClass() == Datatype.CLASS_ENUM) {
				enumIndices.add(i);
			}
			i++;
		}
		if (enumIndices.isEmpty()) {
			enumIndices = null;
		} else {
			// sort by enum string size
			Collections.sort(enumIndices, (i1, i2) -> memberDatatypes[i2].getEnumMembers().length()
					- memberDatatypes[i1].getEnumMembers().length());
		}
		while (enumIndices == null || !enumIndices.isEmpty()) {
			try {
				return H5CompoundDS.create(name, group, dims, null, chunks,
						gzip, memberNames, memberDatatypes, memberRanks,
						memberDims, null);
			} catch (Exception e) {
				// convert enum types to string if they hinder initialization
				if (enumIndices != null && eH == 0
						&& e instanceof HDF5ObjectHeaderException
						&& e.getMessage().equals("Unable to initialize object")) {
					int index = enumIndices.remove(0);
					LogService.getRoot().log(
							Level.WARNING,
							"Converting " + memberNames[index]
									+ " to string and trying again.", e);
					memberDatatypes[index] = new H5Datatype(
							Datatype.CLASS_STRING, Datatype.NATIVE,
							Datatype.NATIVE, Datatype.NATIVE);
				} else {
					LogService.getRoot().log(Level.SEVERE, e.getMessage(), e);
					break;
				}
			}
		}
		return null;
	}

	/**
	 * Creates a {@link Datatype} from a given {@link Attribute} if the
	 * {@link ExampleTable} is HDF5 backed. If the attribute is HDF5 backed too,
	 * the original data type is adopted. Otherwise the type is only based on
	 * the attribute using
	 * {@link #getDatatypeFromAttribute(Attribute, int, int)}.
	 *
	 * @param a
	 *            The attribute to convert.
	 * @param eH
	 *            A flag indicating the enum handling.
	 * @param srcTable
	 *            The HDF5 backed table.
	 * @param size
	 *            The size of the new data set.
	 * @return the data type corresponding to the given attribute.
	 * @see #getDatatypeFromAttribute(Attribute, int,int)
	 */
	private static Datatype getDatatypeFromAttribute(Attribute a, int eH,
			HDF5DatasetExampleTable srcTable, int size) {
		int index = a.getTableIndex();
		Datatype type = null;
		HDF5Attribute h5att = srcTable.h5AttMapping.get(index);
		if (h5att == null) {
			return getDatatypeFromAttribute(a, eH, size);
		}
		if (srcTable.isComp) {
			type = ((CompoundDS) srcTable.dataset).getMemberTypes()[h5att
					.getIndex()];
			type = new H5Datatype(type.toNative());
		} else {
			type = ((ScalarDS) srcTable.dataset).getDatatype();
		}
		if (type.getDatatypeClass() == Datatype.CLASS_ARRAY) {
			type = type.getBasetype();
		}
		// check for enum handling
		if (eH == 2 && type.getDatatypeClass() == Datatype.CLASS_STRING
				|| eH == 1 && type.getDatatypeClass() == Datatype.CLASS_ENUM) {
			return getDatatypeFromAttribute(a, eH, size);
		}
		// if new values were inserted, check for compatibility
		if (srcTable.newValues != null && srcTable.newValues.containsKey(index)) {
			type = checkForUpgrade(a, type);
		}
		return type;
	}

	/**
	 * Checks whether the given {@link Datatype} has to be upgraded to hold
	 * bigger or more precise values specified by an {@link Attribute}. Will
	 * return the original type if no change is needed, otherwise will return a
	 * new type that is able to hold the new values. It also checks if a
	 * previously unsigned attribute now contains negative values to use a new
	 * signed type.
	 *
	 * @param a
	 *            The RapidMiner attribute to check by.
	 * @param type
	 *            The HDF5 data type to check.
	 * @return a data type, able to hold the data.
	 */
	@SuppressWarnings("deprecation")
	static Datatype checkForUpgrade(Attribute a, Datatype type) {
		double min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY;
		// get min and max or return the original
		switch (type.getDatatypeClass()) {
		case Datatype.CLASS_ENUM:
			min = 0;
			max = a.getMapping().size();
			break;
		case Datatype.CLASS_INTEGER:
		case Datatype.CLASS_FLOAT:
			try {
				// TODO don't use deprecation?
				// not possible, since we only see the attribute, not the
				// example set
				min = a.getStatistics(Statistics.MINIMUM);
				max = a.getStatistics(Statistics.MAXIMUM);
			} catch (Exception e) {
			}
			break;
		default:
			return type;
		}
		return checkForUpgrade(type, min, max);

	}

	/**
	 * Checks whether the given {@link Datatype} has to be upgraded to hold
	 * bigger or more precise values specified by the boundaries min and max.
	 * Will return the original type if no change is needed, otherwise will
	 * return a new type that is able to hold the new values. It also checks if
	 * a previously unsigned attribute now contains negative values to use a new
	 * signed type.
	 *
	 * @param type
	 *            The HDF5 data type to check.
	 * @param min
	 *            The minimum boundary.
	 * @param max
	 *            The maximum boundary.
	 * @return a data type, able to hold the data.
	 */
	static Datatype checkForUpgrade(Datatype type, double min, double max) {
		// get number of bytes
		int nBytes;
		if (type.getDatatypeClass() == Datatype.CLASS_FLOAT) {
			nBytes = findNBytesFloat(min, max);
		} else {
			nBytes = findNBytesInt(min, max);
		}
		// check sign
		boolean newSign = min < 0
				&& type.getDatatypeClass() != Datatype.CLASS_FLOAT
				&& type.getDatatypeSign() == Datatype.SIGN_NONE;
		Datatype newType = new H5Datatype(type.getDatatypeClass(), nBytes,
				Datatype.NATIVE, Datatype.NATIVE);
		int tid = newType.toNative();
		if (newSign && type.getDatatypeSize() > H5Datatype.getDatatypeSize(tid)) {
			nBytes = type.getDatatypeSize();
			newType = new H5Datatype(type.getDatatypeClass(), nBytes,
					Datatype.NATIVE, Datatype.NATIVE);
			tid = newType.toNative();
		}
		if (newSign || H5Datatype.getDatatypeSize(tid) > type.getDatatypeSize()) {
			return newType;
		}
		return type;
	}

	/**
	 * Creates a {@link Datatype} from a given {@link Attribute}.
	 *
	 * @param a
	 *            The attribute to convert.
	 * @param eH
	 *            The flag indicating enum handling.
	 * @param size
	 *            The size of the new data set.
	 * @return the data type corresponding to the given attribute.
	 * @see #getDatatypeFromAttribute(Attribute, int, int)
	 */
	@SuppressWarnings("deprecation")
	public static Datatype getDatatypeFromAttribute(Attribute a, int eH,
			int size) {
		int typeID, nBytes = Datatype.NATIVE;
		String enums = null;
		switch (a.getValueType()) {
		case Ontology.BINOMINAL:
		case Ontology.NOMINAL:
		case Ontology.POLYNOMINAL:
		case Ontology.STRING:
		case Ontology.FILE_PATH:
			// create enum or create String data type
			NominalMapping mapping = a.getMapping();
			if (eH == 1 || eH == 0 && useAsEnum(mapping, size)) {
				typeID = Datatype.CLASS_ENUM;
				nBytes = findNBytesInt(0, mapping.size() - 1);
				enums = enumFromMapping(mapping);
			} else {
				typeID = Datatype.CLASS_STRING;
			}
			break;
		case Ontology.DATE:
		case Ontology.TIME:
		case Ontology.DATE_TIME:
			// timestamp as long
			typeID = Datatype.CLASS_INTEGER;
			nBytes = 8;
			break;
		case Ontology.INTEGER:
			typeID = Datatype.CLASS_INTEGER;
			double min = Double.POSITIVE_INFINITY,
			max = Double.NEGATIVE_INFINITY;
			try {
				min = a.getStatistics(Statistics.MINIMUM);
				max = a.getStatistics(Statistics.MAXIMUM);
			} catch (Exception e) {
			}
			nBytes = findNBytesInt(min, max);
			break;
		case Ontology.REAL:
		case Ontology.NUMERICAL:
			typeID = Datatype.CLASS_FLOAT;
			min = Double.POSITIVE_INFINITY;
			max = Double.NEGATIVE_INFINITY;
			try {
				min = a.getStatistics(Statistics.MINIMUM);
				max = a.getStatistics(Statistics.MAXIMUM);
			} catch (Exception e) {
			}
			nBytes = findNBytesFloat(min, max);
			break;
		default:
			typeID = Datatype.CLASS_FLOAT;
			nBytes = 8;
			break;
		}
		Datatype type = new H5Datatype(typeID, nBytes, Datatype.NATIVE,
				Datatype.NATIVE);
		if (enums != null) {
			type.setEnumMembers(enums);
		}
		return type;
	}

	// TODO enum with size 4 does not work?
	// depends on string length of enum members
	/**
	 * Check whether the given enum mapping is best represented as an enum or
	 * plain text regarding memory space.
	 *
	 * @param mapping
	 *            The nominal mapping of the attribute.
	 * @param size
	 *            The size of the new data set.
	 * @return true if the mapping is best represented as an enum.
	 */
	private static boolean useAsEnum(NominalMapping mapping, int size) {
		int n = mapping.size();
		int length = 0;
		for (String s : mapping.getValues()) {
			length += s.length();
		}
		int nBytes = findNBytesInt(0, n - 1);
		// memory for strings (average):
		// size * length / n
		// memory for enum:
		// length + (n + size) * nBytes
		return 0 > length * (n - size) + n * (size + n) * nBytes;
	}

	/**
	 * Finds the number of bytes needed to represent the given minimum and
	 * maximum number as whole numbers. Possible outcomes are 1 for byte, 2 for
	 * short, 4 for int and 8 for long. If min and max are not specified
	 * properly or exceed the bounds of long, {@link Datatype#NATIVE} is
	 * returned.
	 *
	 * @param min
	 *            The minimum number to encode
	 * @param max
	 *            The maximum number to encode
	 * @return the number of bytes needed to represent the numbers bound by min
	 *         and max.
	 */
	private static int findNBytesInt(double min, double max) {
		if (Double.isInfinite(min) || Double.isInfinite(max)) {
			return Datatype.NATIVE;
		}
		if (min >= Byte.MIN_VALUE && max <= Byte.MAX_VALUE) {
			return 1;
		}
		if (min >= Short.MIN_VALUE && max <= Short.MAX_VALUE) {
			return 2;
		}
		if (min >= Integer.MIN_VALUE && max <= Integer.MAX_VALUE) {
			return 4;
		}
		if (min >= Long.MIN_VALUE && max <= Long.MAX_VALUE) {
			return 8;
		}
		return Datatype.NATIVE;
	}

	/**
	 * Finds the number of bytes needed to represent the given minimum and
	 * maximum number as floating point numbers. Possible outcomes are 4 for
	 * float and 8 for double. If min and max are not specified properly,
	 * {@link Datatype#NATIVE} is returned.
	 *
	 * @param min
	 *            The minimum number to encode
	 * @param max
	 *            The maximum number to encode
	 * @return the number of bytes needed to represent the numbers bound by min
	 *         and max.
	 */
	private static int findNBytesFloat(double min, double max) {
		if (Double.isInfinite(min) || Double.isInfinite(max)
				|| Double.isNaN(min) || Double.isNaN(max)) {
			return Datatype.NATIVE;
		}
		min = Math.abs(min);
		max = Math.abs(max);
		// float
		if (min <= Float.MAX_VALUE && max <= Float.MAX_VALUE
				&& min >= Float.MIN_VALUE && max >= Float.MIN_VALUE) {
			return 4;
		}
		return 8;
	}

	/**
	 * Creates an enum for a {@link Datatype} from a {@link NominalMapping}
	 * using indices starting at 0, ignoring the original indexing.
	 *
	 * @param mapping
	 *            The original nominal mapping.
	 * @return the created enum.
	 */
	private static String enumFromMapping(NominalMapping mapping) {
		StringBuilder builder = new StringBuilder();
		int i = 0;
		for (String s : mapping.getValues()) {
			builder.append(s + "=" + i++ + ",");
		}
		return builder.toString();
	}

	@SuppressWarnings("unused")
	private static Dataset createScalar(ExampleTable srcTable, Group group,
			String name, List<Attribute> attributes, long[] chunks, int gzip,
			int size) {
		Datatype datatype;
		if (srcTable instanceof HDF5DatasetExampleTable) {
			datatype = getDatatypeFromAttribute(attributes.get(0), 0,
					(HDF5DatasetExampleTable) srcTable, size);
		} else {
			datatype = getDatatypeFromAttribute(attributes.get(0), 0, size);
		}
		try {
			return H5ScalarDS.create(name, group, datatype,
					new long[] { size }, null, chunks, gzip, null);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Writes the given {@link ExampleSet} to the specified {@link Dataset},
	 * using all available {@link Attribute Attributes} from the example set.
	 * This will return null if the writing was successful, otherwise the
	 * exception that made the writing fail.
	 *
	 * @param source
	 *            The example set to write.
	 * @param target
	 *            The data set to write to.
	 * @return null if the writing succeeded, an Exception otherwise
	 * @see #writeToDataset(ExampleTable, Dataset, List)
	 * @see #writeToDataset(Iterator, Dataset, List, int)
	 */
	public static Exception writeToDataset(ExampleSet source, Dataset target) {
		List<Attribute> attributes = new ArrayList<>();
		Iterator<Attribute> attIter = source.getAttributes().allAttributes();
		// example set => write all attributes
		while (attIter.hasNext()) {
			attributes.add(attIter.next());
		}
		Collections.sort(attributes, attributeComparator);
		// calculate attribute statistics for data type creation
		source.recalculateAllAttributeStatistics();
		if (source instanceof SimpleExampleSet) {
			// all lines used
			return writeToDataset(source.getExampleTable(), target, attributes);
		} else {
			return writeToDataset(source.iterator(), target, attributes,
					source.size());
		}
	}

	/**
	 * Writes the given {@link ExampleTable} to the specified {@link Dataset},
	 * using the selected {@link Attribute Attributes} from the example table.
	 * This will return null if the writing was successful, otherwise the
	 * exception that made the writing fail.
	 *
	 * @param source
	 *            The example table to write.
	 * @param target
	 *            The data set to write to.
	 * @param attributes
	 *            The list of selected attributes.
	 * @return null if the writing succeeded, an Exception otherwise
	 * @see #writeToDataset(HDF5DatasetExampleTable, Dataset, List)
	 * @see #writeToDataset(Iterator, Dataset, List, int)
	 */
	public static Exception writeToDataset(ExampleTable source, Dataset target,
			List<Attribute> attributes) {
		if (attributes == null) {
			attributes = new ArrayList<>(Arrays.asList(source.getAttributes()));
			while (attributes.contains(null)) {
				attributes.remove(null);
			}
		}
		if (source instanceof HDF5DatasetExampleTable) {
			return writeToDataset((HDF5DatasetExampleTable) source, target,
					attributes);
		} else {
			return writeToDataset(source.getDataRowReader(), target,
					attributes, source.size());
		}
	}

	/**
	 * Writes the given {@link HDF5DatasetExampleTable} to the specified
	 * {@link Dataset}, using the selected {@link Attribute Attributes} from the
	 * example table. This method uses the windowing approach already present in
	 * the HDF5 backed table. This will return null if the writing was
	 * successful, otherwise the exception that made the writing fail.
	 *
	 * @param source
	 *            The HDF5 backed example table to write.
	 * @param target
	 *            The data set to write to.
	 * @param attributes
	 *            The list of selected attributes.
	 * @return null if the writing succeeded, an Exception otherwise
	 */
	@SuppressWarnings("unchecked")
	static Exception writeToDataset(HDF5DatasetExampleTable source,
			Dataset target, List<Attribute> attributes) {
		// important: the chunk size of the target set is decisive here, because
		// reading is cheaper than writing
		int windowSize = getTargetWindowSize(source, source.windowSize,
				target.getChunkSize());
		int currentWindowStart = 0;
		long targetStart = target.getStartDims()[0];
		boolean isComp = target instanceof CompoundDS;
		// init stride
		target.getStride();
		Object sourceData, targetData;
		Object sourceArr, targetArr;
		double[] newValues;
		int memberIndex, tmpIndex;

		int length;
		HDF5Attribute h5Att;
		while (currentWindowStart < source.size()) {
			source.setActiveWindow(currentWindowStart);
			target.clear();
			target.getStartDims()[0] = targetStart + currentWindowStart;
			target.getSelectedDims()[0] = source.dataset.getSelectedDims()[0];
			try {
				sourceData = source.dataset.getData();
				targetData = target.getData();
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			memberIndex = 0;
			for (Attribute a : attributes) {
				// get source array
				tmpIndex = a.getTableIndex();
				newValues = null;
				h5Att = source.h5AttMapping.get(tmpIndex);
				if (h5Att != null) {
					if (source.newValues != null
							&& source.newValues.containsKey(tmpIndex)) {
						newValues = source.newValues.get(tmpIndex)[currentWindowStart
								/ windowSize];
					}
					if (source.isComp) {
						tmpIndex = source.usedMembers.indexOf(h5Att.getIndex());
						sourceArr = ((List<Object>) sourceData).get(tmpIndex);
					} else {
						sourceArr = sourceData;
					}
				} else {
					sourceArr = source.newAttributes.get(tmpIndex)[currentWindowStart
							/ windowSize];
				}

				// get target array
				if (isComp) {
					targetArr = ((List<Object>) targetData).get(memberIndex);
				} else {
					targetArr = targetData;
				}

				length = (int) target.getSelectedDims()[0];

				transform(a, h5Att, sourceArr, 0, newValues, targetArr, 0,
						length);
				try {
					target.write();
				} catch (Exception e) {
					e.printStackTrace();
					return e;
				}
				memberIndex++;
			}
			currentWindowStart += windowSize;
		}
		// reset source regarding window size
		source.updateCacheSize();
		source.currentWindowStart = source.size();
		return null;
	}

	/**
	 * If the possible, forces the specified chunk size on the given source if
	 * not null. Otherwise just makes the window size as big as possible and a
	 * multiple of chunk size.
	 *
	 * @param source
	 *            The source HDF5 backed table or null
	 * @param windowSize
	 *            The precomputed window size
	 * @param chunkSize
	 *            A chunk size or null
	 * @return the best possible window size
	 */
	private static int getTargetWindowSize(HDF5DatasetExampleTable source,
			int windowSize, long[] chunkSize) {
		if (chunkSize == null || chunkSize[0] == 0) {
			// no chunking in target
			return windowSize;
		}
		if (windowSize > chunkSize[0] && windowSize % chunkSize[0] != 0) {
			windowSize -= windowSize % chunkSize[0];
			if (source != null) {
				// force chunking of target on source
				source.windowSize = windowSize;
				// will definitely reset hyper slab when setActiveWindow is
				// called
				source.currentWindowStart = source.size();
				// reorganize static data
				source.reorganizeStaticData();
			}
		}
		return windowSize;
	}

	/**
	 * Transforms the given data from the source array to the specified target
	 * array based on the RapidMiner {@link Attribute}, if that attribute is
	 * backed by HDF5 and possibly new values for that attribute.
	 *
	 * @param att
	 *            The RapidMiner attribute.
	 * @param h5Att
	 *            The associated HDF backed attribute or null.
	 * @param srcArr
	 *            The source array to write.
	 * @param srcOff
	 *            The offset on the source array.
	 * @param newValues
	 *            The new values for this attribute.
	 * @param trgArr
	 *            The target array to write to.
	 * @param trgOff
	 *            The offset on the target array.
	 * @param length
	 *            The length of the section to write.
	 * @throws IllegalArgumentException
	 *             if neither the array types are equal, nor the source array is
	 *             of type double.
	 * @see ConverterUtil
	 */
	private static void transform(Attribute att, HDF5Attribute h5Att,
			Object srcArr, int srcOff, double[] newValues, Object trgArr,
			int trgOff, int length) {
		if (h5Att != null && h5Att.isArray()) {
			// reduce if array
			ConverterUtil.reduce(att, h5Att, srcArr, srcOff, trgArr, trgOff,
					length);
		} else {
			if (srcArr.getClass() == trgArr.getClass()) {
				// simple copy if same array type
				System.arraycopy(srcArr, srcOff, trgArr, trgOff, length);
			} else {
				// convert otherwise
				ConverterUtil.convert(att, h5Att, srcArr, srcOff, trgArr,
						trgOff, length);
			}
		}
		transformNewValues(att, h5Att, newValues, 0, trgArr, trgOff, length);
	}

	/**
	 * Writes the given new values to the target array if necessary. Infinite
	 * entries are ignored. If the new values array is null, this method does
	 * nothing.
	 *
	 * @param att
	 *            The RapidMiner attribute.
	 * @param h5Att
	 *            The associated HDF backed attribute.
	 * @param newValues
	 *            The new values for this attribute.
	 * @param newOff
	 *            The offset for the new value array.
	 * @param trgArr
	 *            The target array to write to.
	 * @param trgOff
	 *            The offset on the target array.
	 * @param length
	 *            The length of the section to write.
	 */
	static void transformNewValues(Attribute att, HDF5Attribute h5Att,
			double[] newValues, int newOff, Object trgArr, int trgOff,
			int length) {
		if (newValues == null) {
			return;
		}
		// true if the enum is not interpreted as a string; the value has to be
		// transformed from RM representation to HDF representation
		boolean transformPoly = h5Att.isPolynominal()
				&& !(trgArr instanceof String[]);
		// check every value
		double value;
		int trgOffTmp;
		for (int i = 0; i < length; i++) {
			value = newValues[newOff + i];
			// ignore if infinite
			if (Double.isInfinite(value)) {
				continue;
			}
			if (transformPoly) {
				// transform RM mapping to HDF mapping
				value = h5Att.map2HDF((int) value);
			}
			// array => subindex
			trgOffTmp = trgOff
					+ (h5Att.isArray() ? h5Att.getSubindices(i)[0] : i);
			// convert to int or String
			ConverterUtil.convert(att, new double[] { value }, 0, trgArr,
					trgOffTmp, 1);
		}
	}

	/**
	 * Writes from the given {@link Iterator} to the specified {@link Dataset},
	 * using the selected {@link Attribute Attributes} from the underlying
	 * example table. This is a row by row write implementation for special
	 * example sets or example tables that are not HDF5 backed. This method
	 * expects either an {@link Example} {@link Iterator} or a
	 * {@link DataRowReader}. This will return null if the writing was
	 * successful, otherwise the exception that made the writing fail.
	 *
	 * @param iterator
	 *            The example iterator or data row reader to write.
	 * @param target
	 *            The target data set to write to.
	 * @param attributes
	 *            The selected attributes.
	 * @param size
	 *            The number of examples/data rows in the iterator.
	 * @return null if the writing succeeded, an Exception otherwise
	 */
	@SuppressWarnings("unchecked")
	private static <E> Exception writeToDataset(Iterator<E> iterator,
			Dataset target, List<Attribute> attributes, int size) {
		int windowSize = (int) (HDF5DatasetExampleTable.CACHE_MEMORY
				* HDF5DatasetExampleTable.WINDOW_CACHE_RATIO / attributes
				.size());
		if (windowSize > size) {
			windowSize = size;
		} else {
			windowSize = getTargetWindowSize(null, windowSize,
					target.getChunkSize());
		}
		int currentWindowStart = 0;
		boolean isComp = target instanceof CompoundDS;
		target.getStride();

		Object targetData;
		List<Object> dataList = null;
		Object targetArr = null;
		E next;
		DataRow data;
		int memberIndex, relIndex;

		while (currentWindowStart < size) {
			target.clear();
			target.getStartDims()[0] = currentWindowStart;
			target.getSelectedDims()[0] = windowSize;
			try {
				targetData = target.getData();
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			if (isComp) {
				dataList = (List<Object>) targetData;
			} else {
				targetArr = targetData;
			}
			for (relIndex = 0; relIndex < windowSize; relIndex++) {
				next = iterator.next();
				// get data row
				if (next instanceof Example) {
					data = ((Example) next).getDataRow();
				} else {
					data = (DataRow) next;
				}
				memberIndex = 0;
				for (Attribute a : attributes) {
					if (isComp) {
						targetArr = dataList.get(memberIndex);
					}
					ConverterUtil.convert(a, new double[] { data.get(a) }, 0,
							targetArr, relIndex, 1);
					memberIndex++;
				}
			}
			try {
				target.write();
			} catch (Exception e) {
				e.printStackTrace();
				return e;
			}
			currentWindowStart += windowSize;
			if (currentWindowStart + windowSize > size) {
				windowSize = size - currentWindowStart;
			}
		}
		return null;
	}

}
