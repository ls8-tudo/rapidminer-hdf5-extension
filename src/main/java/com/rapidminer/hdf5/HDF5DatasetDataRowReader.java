/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import java.util.NoSuchElementException;

import com.rapidminer.example.table.AbstractDataRowReader;
import com.rapidminer.example.table.DataRow;
import com.rapidminer.example.table.DataRowReader;
import com.rapidminer.example.table.ExampleTable;

/**
 * This simple {@link DataRowReader} reads {@link HDF5DatasetDataRow
 * HDF5DatasetDataRows} from an {@link HDF5DatasetExampleTable}.
 * 
 * @author Jan Czogalla
 * 
 */
public class HDF5DatasetDataRowReader implements DataRowReader {

	/**
	 * The HDF5 backed {@link ExampleTable} backing this reader
	 */
	private HDF5DatasetExampleTable table;
	/**
	 * The current index of this reader
	 */
	private int current;

	public HDF5DatasetDataRowReader(HDF5DatasetExampleTable table) {
		this.table = table;
	}

	@Override
	public boolean hasNext() {
		return current < table.size();
	}

	@Override
	public DataRow next() {
		if (!hasNext()) {
			throw new NoSuchElementException();
		}
		table.preload(current);
		return table.getDataRow(current++);
	}

	/**
	 * @see AbstractDataRowReader#remove()
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException(
				"The method 'remove' is not supported by DataRowReaders!");
	}

}
