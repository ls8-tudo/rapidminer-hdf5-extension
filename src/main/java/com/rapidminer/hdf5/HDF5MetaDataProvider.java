/*******************************************************************************
 * RapidMiner
 *
 * Copyright (C) 2001-2014 by RapidMiner and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * http://rapidminer.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5;

import java.util.ArrayList;
import java.util.List;

import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.MetaDataChangeListener;
import com.rapidminer.operator.ports.metadata.MetaData;
import com.rapidminer.parameter.MetaDataProvider;
import com.rapidminer.parameter.ParameterTypeAttribute;

/**
 * A simple adapter for {@link ParameterTypeAttribute} to get meta data directly
 * from the associated {@link HDF5Reader}.
 * 
 * @author Jan Czogalla
 * 
 */
public class HDF5MetaDataProvider implements MetaDataProvider {

	/**
	 * The associated {@link HDF5Reader}.
	 */
	private HDF5Reader reader;

	public HDF5MetaDataProvider(HDF5Reader reader) {
		this.reader = reader;
	}

	private List<MetaDataChangeListener> listeners = new ArrayList<>();

	@Override
	public void addMetaDataChangeListener(MetaDataChangeListener l) {
		listeners.add(l);
	}

	@Override
	public void removeMetaDataChangeListener(MetaDataChangeListener l) {
		listeners.remove(l);
	}

	@Override
	public MetaData getMetaData() {
		try {
			return reader.getGeneratedMetaData();
		} catch (OperatorException e) {
			return null;
		}
	}

}
