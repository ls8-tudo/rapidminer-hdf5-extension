/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5.util;

import java.lang.reflect.Array;
import java.util.Arrays;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Tools;
import com.rapidminer.example.table.NominalMapping;
import com.rapidminer.hdf5.HDF5Attribute;

/**
 * A class containing converter methods for data rows.
 * 
 * @author Jan Czogalla
 * 
 */
public class ConverterUtil {

	/**
	 * Converts an array of type A to an array of type B, where A and B are
	 * either primitive or String respectively. If A does not equal B and
	 * neither is double, the conversion will be done using a double array as an
	 * intermediate step.
	 * 
	 * @param a
	 *            The attribute corresponding to the source array
	 * @param h5a
	 *            The HDF attribute corresponding to the source array or null
	 * @param srcArr
	 *            The source array
	 * @param srcOff
	 *            The offset of the source array
	 * @param trgArr
	 *            The target array
	 * @param trgOff
	 *            The offset of the target array
	 * @param length
	 *            The number of data points to convert
	 * @see #convert(Attribute, double[], int, Object, int, int)
	 * @see #convert(Attribute, HDF5Attribute, Object, int, double[], int, int)
	 */
	public static void convert(Attribute a, HDF5Attribute h5a, Object srcArr,
			int srcOff, Object trgArr, int trgOff, int length) {
		// simple copy
		if (srcArr.getClass() == trgArr.getClass()) {
			System.arraycopy(srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		// transform from double
		if (srcArr instanceof double[]) {
			convert(a, (double[]) srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		// transform to double
		if (trgArr instanceof double[]) {
			convert(a, h5a, srcArr, srcOff, (double[]) trgArr, trgOff, length);
			return;
		}
		// convert with double array as intermediate
		double[] tmp = new double[length];
		convert(a, h5a, srcArr, srcOff, tmp, 0, length);
		convert(a, tmp, 0, trgArr, trgOff, length);
	}

	/**
	 * Converts the given double array to an array composed of another type.
	 * Conversion might be influenced by the corresponding attribute.
	 * 
	 * @param a
	 *            The attribute corresponding to the source array
	 * @param srcArr
	 *            The source array
	 * @param srcOff
	 *            The offset of the source array
	 * @param trgArr
	 *            The target array
	 * @param trgOff
	 *            The offset of the target array
	 * @param length
	 *            The number of data points to convert
	 */
	public static void convert(Attribute a, double[] srcArr, int srcOff,
			Object trgArr, int trgOff, int length) {
		if (trgArr instanceof double[]) {
			System.arraycopy(srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		if (trgArr instanceof float[]) {
			convert(srcArr, srcOff, (float[]) trgArr, trgOff, length);
			return;
		}
		if (trgArr instanceof long[]) {
			convert(srcArr, srcOff, (long[]) trgArr, trgOff, length);
			return;
		}
		if (trgArr instanceof int[]) {
			convert(srcArr, srcOff, (int[]) trgArr, trgOff, length);
			return;
		}
		if (trgArr instanceof short[]) {
			convert(srcArr, srcOff, (short[]) trgArr, trgOff, length);
			return;
		}
		if (trgArr instanceof byte[]) {
			convert(srcArr, srcOff, (byte[]) trgArr, trgOff, length);
			return;
		}
		if (trgArr instanceof String[]) {
			convert(a, srcArr, srcOff, (String[]) trgArr, trgOff, length);
			return;
		}
	}

	/**
	 * Convert double array to float array with primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, double[], int, Object, int, int)
	 */
	static void convert(double[] srcArr, int srcOff, float[] trgArr,
			int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = (float) srcArr[srcOff + i];
		}
	}

	/**
	 * Convert double array to long array with primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, double[], int, Object, int, int)
	 */
	static void convert(double[] srcArr, int srcOff, long[] trgArr, int trgOff,
			int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = (long) srcArr[srcOff + i];
		}
	}

	/**
	 * Convert double array to integer array with primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, double[], int, Object, int, int)
	 */
	static void convert(double[] srcArr, int srcOff, int[] trgArr, int trgOff,
			int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = (int) srcArr[srcOff + i];
		}
	}

	/**
	 * Convert double array to short array with primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, double[], int, Object, int, int)
	 */
	static void convert(double[] srcArr, int srcOff, short[] trgArr,
			int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = (short) srcArr[srcOff + i];
		}
	}

	/**
	 * Convert double array to byte array with primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, double[], int, Object, int, int)
	 */
	static void convert(double[] srcArr, int srcOff, byte[] trgArr, int trgOff,
			int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = (byte) srcArr[srcOff + i];
		}
	}

	/**
	 * Convert double array to String array using the
	 * {@link Attribute#getAsString(double, int, boolean) Attribute.getAsString}
	 * method on the individual data points.
	 * 
	 * @see ConverterUtil#convert(Attribute, double[], int, Object, int, int)
	 */
	static void convert(Attribute a, double[] srcArr, int srcOff,
			String[] trgArr, int trgOff, int length) {
		if (a.isDateTime() || a.isNominal()) {
			for (int i = 0; i < length; i++) {
				trgArr[trgOff + i] = a
						.getAsString(srcArr[srcOff + i], 0, false);
			}
		} else {
			throw new IllegalArgumentException("Attribute type not supported: "
					+ a);
		}
	}

	/**
	 * Converts the given primitive or String array to a double array.
	 * Conversion might be influenced by the corresponding attribute.
	 * 
	 * @param a
	 *            The attribute corresponding to the source array
	 * @param h5a
	 *            The HDF attribute corresponding to the source array or null
	 * @param srcArr
	 *            The source array
	 * @param srcOff
	 *            The offset of the source array
	 * @param trgArr
	 *            The target array
	 * @param trgOff
	 *            The offset of the target array
	 * @param length
	 *            The number of data points to convert
	 */
	public static void convert(Attribute a, HDF5Attribute h5a, Object srcArr,
			int srcOff, double[] trgArr, int trgOff, int length) {
		if (srcArr instanceof double[]) {
			System.arraycopy(srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		if (srcArr instanceof float[]) {
			convert((float[]) srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		if (srcArr instanceof long[]) {
			convert((long[]) srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		if (srcArr instanceof int[]) {
			convert((int[]) srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		if (srcArr instanceof short[]) {
			convert((short[]) srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		if (srcArr instanceof byte[]) {
			convert((byte[]) srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
		if (srcArr instanceof String[]) {
			convert(a, h5a, (String[]) srcArr, srcOff, trgArr, trgOff, length);
			return;
		}
	}

	/**
	 * Convert float array to double array with implicit primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, HDF5Attribute, Object, int,
	 *      double[], int, int)
	 */
	static void convert(float[] srcArr, int srcOff, double[] trgArr,
			int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[srcOff + i];
		}
	}

	/**
	 * Convert long array to double array with implicit primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, HDF5Attribute, Object, int,
	 *      double[], int, int)
	 */
	static void convert(long[] srcArr, int srcOff, double[] trgArr, int trgOff,
			int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[srcOff + i];
		}
	}

	/**
	 * Convert int array to double array with implicit primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, HDF5Attribute, Object, int,
	 *      double[], int, int)
	 */
	static void convert(int[] srcArr, int srcOff, double[] trgArr, int trgOff,
			int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[srcOff + i];
		}
	}

	/**
	 * Convert short array to double array with implicit primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, HDF5Attribute, Object, int,
	 *      double[], int, int)
	 */
	static void convert(short[] srcArr, int srcOff, double[] trgArr,
			int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[srcOff + i];
		}
	}

	/**
	 * Convert byte array to double array with implicit primitive casting
	 * 
	 * @see ConverterUtil#convert(Attribute, HDF5Attribute, Object, int,
	 *      double[], int, int)
	 */
	static void convert(byte[] srcArr, int srcOff, double[] trgArr, int trgOff,
			int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[srcOff + i];
		}
	}

	/**
	 * Convert String array to double array using the
	 * {@link NominalMapping#getIndex(String)} method and the mapping of the
	 * {@link HDF5Attribute} if existent on the individual data points.
	 * 
	 * @see ConverterUtil#convert(Attribute, HDF5Attribute, Object, int,
	 *      double[], int, int)
	 */
	static void convert(Attribute a, HDF5Attribute h5a, String[] srcArr,
			int srcOff, double[] trgArr, int trgOff, int length) {
		if (a.isDateTime() || a.isNominal()) {
			int value;
			for (int i = 0; i < length; i++) {
				value = a.getMapping().getIndex(srcArr[srcOff + i]);
				if (h5a != null) {
					value = h5a.map2HDF(value);
				}
				trgArr[trgOff + i] = value;
			}
		} else {
			throw new IllegalArgumentException("Attribute type not supported: "
					+ a);
		}
	}

	/**
	 * Extracts a single column from the source array (interlaced columns). This
	 * requires both the source and target array to be of the same type.
	 * 
	 * @param att
	 * 
	 * @param h5Att
	 *            The HDF5 attribute corresponding to the source array
	 * @param srcArr
	 *            The source array
	 * @param srcOff
	 *            The offset of the source array
	 * @param trgArr
	 *            The target array
	 * @param trgOff
	 *            The offset of the target array
	 * @param length
	 *            The number of data points to convert
	 */
	public static void reduce(Attribute att, HDF5Attribute h5Att,
			Object srcArr, int srcOff, Object trgArr, int trgOff, int length) {
		if (srcArr.getClass() != trgArr.getClass()) {
			// not the same type => use src type array as intermediate
			// A[][] -> A[] -> B[]
			Object tmpArr = Array.newInstance(srcArr.getClass()
					.getComponentType(), length);
			reduce(att, h5Att, srcArr, srcOff, tmpArr, 0, length);
			convert(att, h5Att, tmpArr, 0, trgArr, trgOff, length);
			return;
		}
		if (srcArr instanceof double[]) {
			reduce(h5Att, (double[]) srcArr, srcOff, (double[]) trgArr, trgOff,
					length);
			return;
		}
		if (srcArr instanceof float[]) {
			reduce(h5Att, (float[]) srcArr, srcOff, (float[]) trgArr, trgOff,
					length);
			return;
		}
		if (srcArr instanceof long[]) {
			reduce(h5Att, (long[]) srcArr, srcOff, (long[]) trgArr, trgOff,
					length);
			return;
		}
		if (srcArr instanceof int[]) {
			reduce(h5Att, (int[]) srcArr, srcOff, (int[]) trgArr, trgOff,
					length);
			return;
		}
		if (srcArr instanceof short[]) {
			reduce(h5Att, (short[]) srcArr, srcOff, (short[]) trgArr, trgOff,
					length);
			return;
		}
		if (srcArr instanceof byte[]) {
			reduce(h5Att, (byte[]) srcArr, srcOff, (byte[]) trgArr, trgOff,
					length);
			return;
		}
		if (srcArr instanceof String[]) {
			reduce(h5Att, (String[]) srcArr, srcOff, (String[]) trgArr, trgOff,
					length);
			return;
		}
	}

	/**
	 * Extracts a single column from the source double array (interlaced
	 * columns).
	 * 
	 * @see ConverterUtil#reduce(Attribute, HDF5Attribute, Object, int, Object,
	 *      int, int)
	 */
	static void reduce(HDF5Attribute h5Att, double[] srcArr, int srcOff,
			double[] trgArr, int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[h5Att.getSubindices(srcOff + i)[0]];
		}
	}

	/**
	 * Extracts a single column from the source float array (interlaced
	 * columns).
	 * 
	 * @see ConverterUtil#reduce(Attribute, HDF5Attribute, Object, int, Object,
	 *      int, int)
	 */
	static void reduce(HDF5Attribute h5Att, float[] srcArr, int srcOff,
			float[] trgArr, int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[h5Att.getSubindices(srcOff + i)[0]];
		}
	}

	/**
	 * Extracts a single column from the source long array (interlaced columns).
	 * 
	 * @see ConverterUtil#reduce(Attribute, HDF5Attribute, Object, int, Object,
	 *      int, int)
	 */
	static void reduce(HDF5Attribute h5Att, long[] srcArr, int srcOff,
			long[] trgArr, int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[h5Att.getSubindices(srcOff + i)[0]];
		}
	}

	/**
	 * Extracts a single column from the source integer array (interlaced
	 * columns).
	 * 
	 * @see ConverterUtil#reduce(Attribute, HDF5Attribute, Object, int, Object,
	 *      int, int)
	 */
	static void reduce(HDF5Attribute h5Att, int[] srcArr, int srcOff,
			int[] trgArr, int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[h5Att.getSubindices(srcOff + i)[0]];
		}
	}

	/**
	 * Extracts a single column from the source short array (interlaced
	 * columns).
	 * 
	 * @see ConverterUtil#reduce(Attribute, HDF5Attribute, Object, int, Object,
	 *      int, int)
	 */
	static void reduce(HDF5Attribute h5Att, short[] srcArr, int srcOff,
			short[] trgArr, int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[h5Att.getSubindices(srcOff + i)[0]];
		}
	}

	/**
	 * Extracts a single column from the source byte array (interlaced columns).
	 * 
	 * @see ConverterUtil#reduce(Attribute, HDF5Attribute, Object, int, Object,
	 *      int, int)
	 */
	static void reduce(HDF5Attribute h5Att, byte[] srcArr, int srcOff,
			byte[] trgArr, int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[h5Att.getSubindices(srcOff + i)[0]];
		}
	}

	/**
	 * Extracts a single column from the source String array (interlaced
	 * columns).
	 * 
	 * @see ConverterUtil#reduce(Attribute, HDF5Attribute, Object, int, Object,
	 *      int, int)
	 */
	static void reduce(HDF5Attribute h5Att, String[] srcArr, int srcOff,
			String[] trgArr, int trgOff, int length) {
		for (int i = 0; i < length; i++) {
			trgArr[trgOff + i] = srcArr[h5Att.getSubindices(srcOff + i)[0]];
		}
	}

	/**
	 * Reorganizes a given two-dimensional encoding of a one-dimensional array
	 * into a new two-dimensional encoding. The new encoding is fixed in the
	 * first dimension, leaving the second dimension to be calculated. Then,
	 * data is copied section-wise into the new structure.
	 * 
	 * @param source
	 *            The encoding to be reorganized.
	 * @param target
	 *            The target structure of the new encoding.
	 * @param size
	 *            The total number of values to be encoded.
	 * @param fillValue
	 *            The optional fill value for the new encoding.
	 */
	public static void reorganize(double[][] source, double[][] target,
			int size, Double fillValue) {
		// source is a x b, target is c x d
		int b = 0;
		for (double[] arr : source) {
			if (arr != null) {
				b = arr.length;
				break;
			}
		}
		if (b == 0) {
			return;
		}
		int c = target.length;
		int d = (int) Math.ceil(((double) size) / c);
		int i, k, n, old;
		n = 0;
		i = k = 1;
		int length;
		int srcOff;
		boolean copy;
		while (n < size) {
			// old mark and new mark of sections
			old = n;
			n = Math.min(i * b, k * d);
			n = Math.min(n, size);
			length = n - old;
			// only if the source exists in the given section, copy its content
			if (source[i - 1] != null) {
				srcOff = old % b;
				// if section contains only fill values, don't copy
				if (fillValue != null) {
					copy = false;
					for (int l = 0; l < length; l++) {
						if (!Tools.isDefault(fillValue, source[i - 1][srcOff
								+ l])) {
							copy = true;
							break;
						}
					}
				} else {
					copy = true;
				}
				if (copy) {
					// if target section does not exist, create
					if (target[k - 1] == null) {
						target[k - 1] = new double[d];
						if (fillValue != null) {
							Arrays.fill(target[k - 1], fillValue);
						}
					}
					System.arraycopy(source[i - 1], srcOff, target[k - 1], old
							% d, length);
				}
			}
			// shift encodings
			if (n % b == 0) {
				i++;
			}
			if (n % d == 0) {
				k++;
			}
		}
		// trim the last part of the data to size
		if (c * d > size && target[c - 1] != null
				&& target[c - 1].length != size % d) {
			double[] rest = new double[size % d];
			System.arraycopy(target[c - 1], 0, rest, 0, rest.length);
			target[c - 1] = rest;
		}
	}
}
