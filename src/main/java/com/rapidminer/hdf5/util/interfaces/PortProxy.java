/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5.util.interfaces;

import com.rapidminer.operator.ports.Port;
import com.rapidminer.operator.ports.metadata.MetaData;

/**
 * A compatibility interface to allow getting type checked {@link MetaData} from
 * a {@link Port}. The {@link Port#getMetaData(Class)} method was introduced in
 * RM 6.0.
 * 
 * @author Jan Czogalla
 * @since 5.5.6
 *
 */
public interface PortProxy {

	/**
	 * Returns a type checked {@link MetaData} object or throws an exception if
	 * the detected meta data does not match the desired class.
	 * 
	 * @param p
	 *            The port to get the meta data from
	 * @param mdc
	 *            The desired {@link MetaData} class
	 * @return the checked meta data
	 * @throws Exception
	 * 
	 * @see {@link Port#getMetaData()}, {@link Port#getMetaData(Class)}
	 */
	<T extends MetaData> T getMetaData(Port p, Class<T> mdc) throws Exception;

}
