/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5.util.interfaces;

import java.io.File;

/**
 * A compatibility interface to get the parent directory for the hdflib folder
 * that contains the native HDF5 libraries. Since the {@link SecurityManager}
 * update in RM 7.2, file access is restricted.
 * 
 * @author Jan Czogalla
 * @since 5.5.6
 */
public interface LibDirProxy {

	/**
	 * Returns the parent directory for the hdflib folder.
	 * 
	 */
	File getLibDirParent();

}
