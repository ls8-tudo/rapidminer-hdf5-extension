/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5.util.interfaces;

import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.table.ExampleTable;
import com.rapidminer.hdf5.HDF5DatasetExampleTable;

/**
 * A compatibility interface to allow for the new creation of example sets
 * introduced with RM 7.3
 * 
 * @author Jan Czogalla
 * @since 5.5.6
 *
 */
public interface MaterializeDataProxy {

	/**
	 * Converts the specified HDF5 backed {@link HDF5DatasetExampleTable} to an
	 * in-memory {@link ExampleTable} and returns it as an {@link ExampleSet}.
	 * 
	 * @param table
	 *            The HDF5 backed table
	 * @return the in-memory example set
	 */
	ExampleSet createInMemory(HDF5DatasetExampleTable table);

}
