/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5.util;

import java.io.File;

import com.rapidminer.example.ExampleSet;
import com.rapidminer.hdf5.HDF5DatasetExampleTable;
import com.rapidminer.hdf5.util.classes.LibDirOld;
import com.rapidminer.hdf5.util.classes.LibDirSevenTwo;
import com.rapidminer.hdf5.util.classes.MaterializeDataOld;
import com.rapidminer.hdf5.util.classes.MaterialzeDataSevenThree;
import com.rapidminer.hdf5.util.classes.PortOld;
import com.rapidminer.hdf5.util.classes.PortSixZero;
import com.rapidminer.hdf5.util.interfaces.LibDirProxy;
import com.rapidminer.hdf5.util.interfaces.MaterializeDataProxy;
import com.rapidminer.hdf5.util.interfaces.PortProxy;
import com.rapidminer.operator.ports.Port;
import com.rapidminer.operator.ports.metadata.MetaData;
import com.rapidminer.tools.FileSystemService;

/**
 * The compatibility class combines all compatibility interfaces for easy
 * access.
 * 
 * @author Jan Czogalla
 * @since 5.5.6
 *
 * @see MaterializeDataProxy
 * @see PortProxy
 * @see LibDirProxy
 */
public class Compatiblity implements MaterializeDataProxy, PortProxy, LibDirProxy {

	/**
	 * The {@link MaterializeDataProxy} instance
	 */
	private static MaterializeDataProxy matDataProxy;
	static {
		// since 7.3 there is a new mechanic to create data tables
		try {
			Class.forName("com.rapidminer.example.utils.ExampleSets");
			matDataProxy = new MaterialzeDataSevenThree();
		} catch (ClassNotFoundException e) {
			matDataProxy = new MaterializeDataOld();
		}
	}

	/**
	 * The {@link PortProxy} instance
	 */
	private static PortProxy portProxy;
	static {
		// since 6.0, there is a generic/checked method to get meta data from a
		// port
		try {
			Port.class.getMethod("getMetaData", Class.class);
			portProxy = new PortSixZero();
		} catch (NoSuchMethodException e) {
			portProxy = new PortOld();
		}
	}

	/**
	 * The {@link LibDirProxy} instance
	 */
	private static LibDirProxy libDirProxy;
	static {
		// since 7.2 rules are strict regarding file access
		try {
			FileSystemService.class.getMethod("getPluginRapidMinerDir", String.class);
			libDirProxy = new LibDirSevenTwo();
		} catch (NoSuchMethodException e) {
			libDirProxy = new LibDirOld();
		}
	}

	/**
	 * The singleton instance.
	 */
	private static final Compatiblity instance = new Compatiblity();

	// private constructor since this is a singleton
	private Compatiblity() {
	}

	/**
	 * Returns the singleton instance
	 */
	public static Compatiblity getDefault() {
		return instance;
	}

	@Override
	public ExampleSet createInMemory(HDF5DatasetExampleTable table) {
		return matDataProxy.createInMemory(table);
	}

	@Override
	public <T extends MetaData> T getMetaData(Port p, Class<T> mdc) throws Exception {
		return portProxy.getMetaData(p, mdc);
	}

	@Override
	public File getLibDirParent() {
		return libDirProxy.getLibDirParent();
	}

}
