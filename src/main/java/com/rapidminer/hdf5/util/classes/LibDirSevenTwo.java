/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 * 
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 * 
 * Complete list of developers available at our web site:
 * 
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF5
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer.hdf5.util.classes;

import java.io.File;

import com.rapidminer.PluginInitHDF5;
import com.rapidminer.hdf5.util.interfaces.LibDirProxy;
import com.rapidminer.tools.FileSystemService;

/**
 * This implementation of {@link LibDirProxy} uses the new
 * {@link FileSystemService#getPluginRapidMinerDir(String)} as the parent
 * directory.
 * 
 * @author Jan Czogalla
 * @since 5.5.6
 *
 */
public class LibDirSevenTwo implements LibDirProxy {

	@Override
	public File getLibDirParent() {
		return FileSystemService.getPluginRapidMinerDir(PluginInitHDF5.HDF_KEY);
	}

}
