/*******************************************************************************
 * TU Dortmund, Computer Science VIII
 *
 * Copyright (C) 2014-2016 by LS 8 and the contributors
 *
 * Complete list of developers available at our web site:
 *
 * 	www-ai.cs.uni-dortmund.de/SOFTWARE/HDF
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 ******************************************************************************/

package com.rapidminer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Enumeration;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.rapidminer.gui.MainFrame;
import com.rapidminer.hdf5.util.Compatiblity;
import com.rapidminer.repository.RepositoryManager;
import com.rapidminer.repository.resource.ResourceRepository;
import com.rapidminer.tools.FileSystemService;
import com.rapidminer.tools.GroupTree;
import com.rapidminer.tools.OperatorService;
import com.rapidminer.tools.ParameterService;
import com.rapidminer.tools.plugin.Plugin;

/**
 * This class provides hooks for initialization
 *
 * @author Jan Czogalla
 */
public class PluginInitHDF5 {

	private static final String HDF_LIB_DIR = "hdflib";
	private static final String JHDF_VERSION = "2.11.0";
	private static final String[] OS_NAMES = new String[] { "lin", "win32", "win64", "mac", "sol" };
	public static final String HDF_KEY = "rmx_hdf5";
	private static boolean removeLibPath;
	private static ResourceRepository rep;
	private static File libDir;

	/**
	 * This method will be called directly after the extension is initialized.
	 * This is the first hook during start up. No initialization of the
	 * operators or renderers has taken place when this is called.
	 */
	public static void initPlugin() {
		Plugin hdf5 = Plugin.getPluginByExtensionId(HDF_KEY);
		if (hdf5 == null) {
			// not possible in RM
			throw new RuntimeException("HDF plugin not found.");
		}
		// copying native libraries if needed
		libDir = createLibDir();
		if (libDir == null) {
			throw new RuntimeException("Creating hdflib directory failed");
		}

		try {
			copyLibFiles(hdf5, libDir);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Copying hdf lib files failed");
		}
		try {
			removeLibPath = addLibraryPath(libDir.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Adding hdf lib path failed");
		}
	}

	/**
	 * Creating or retrieving the directory where the native libraries should be
	 * located.
	 *
	 * @return The directory for the native libraries
	 */
	private static File createLibDir() {
		// create "hdflib/version" directory if not already existent
		File libDir;
		File parentDir = getParentDir();
		libDir = new File(parentDir, HDF_LIB_DIR + File.separator + JHDF_VERSION);
		File parent = libDir.getParentFile();
		if (parent.exists()) {
			// remove old entries that were directly in hdflib folder
			for (File child : parent.listFiles()) {
				if (child.isDirectory()) {
					continue;
				}
				if (!child.delete()) {
					child.deleteOnExit();
				}
			}
			// remove old libpath if still present
			try {
				removeLibraryPath(HDF_LIB_DIR);
			} catch (Exception e) {
			}
		}
		if (!libDir.exists()) {
			if (!libDir.mkdirs()) {
				return null;
			}
		} else if (!libDir.isDirectory()) {
			return null;
		}
		return libDir;
	}

	/**
	 * Returns the parent directory for the HDF library files. This goes through
	 * the following locations in order: {@link Plugin#getPluginLocation()},
	 * parameter "com.rapidanalytics.plugindir",
	 * {@link FileSystemService#getUserRapidMinerDir()}, system parameter
	 * "java.io.tmp". The first to be resolvable to an existing location will be
	 * returned, otherwise an exception is thrown.
	 *
	 * @return the parent directory for the HDF libraries
	 * @throws RuntimeException
	 *             if no location could be found
	 */
	private static File getParentDir() {
		File dir = null;
		// default plugin or extension workspace location
		try {
			dir = Compatiblity.getDefault().getLibDirParent();
		} catch (Exception e) {
		}
		if (dir != null && dir.exists()) {
			return dir;
		}
		// server plugin location
		String locationProperty = ParameterService.getParameterValue("com.rapidanalytics.plugindir");
		if (locationProperty != null && !locationProperty.isEmpty()) {
			dir = new File(locationProperty);
			if (dir.exists()) {
				return dir;
			}
		}
		// RapidMiner user directory
		dir = FileSystemService.getUserRapidMinerDir();
		if (dir != null && dir.exists()) {
			return dir;
		}
		// default tmp directory
		dir = new File(System.getProperty("java.io.tmpdir"));
		if (dir.exists()) {
			return dir;
		}
		throw new RuntimeException(
				"Could not find parent directory for hdf libraries");
	}

	/**
	 * Copy the native libraries to the specified directory.
	 *
	 * @param hdf5
	 *            - The instance of the HDF5 extension.
	 * @param libDir
	 *            - The directory to copy to.
	 * @throws Exception
	 *             if an error occurs while copying the files.
	 */
	private static void copyLibFiles(Plugin hdf5, File libDir) throws Exception {
		JarFile file = hdf5.getArchive();
		String libFolder = getLibFolder(file);
		if (libFolder == null) {
			throw new Exception(
					"Native library files for this OS could not be found in jar file.");
		}
		String name;
		Enumeration<JarEntry> entries = file.entries();
		JarEntry entry;
		while (entries.hasMoreElements()) {
			entry = entries.nextElement();
			if (entry.isDirectory()) {
				continue;
			}
			name = entry.getName();
			if (name.startsWith(libFolder)) {
				name = name.substring(libFolder.length() + 1);
				File target = new File(libDir, name);
				copyLibFile(file.getInputStream(entry), target);
			}
		}
	}

	/**
	 * Gets the folder of native libraries suitable for this OS. Will return the
	 * simple {@link #HDF_LIB_DIR} if this is an OS specific build, otherwise
	 * returns the specific folder.
	 *
	 * @param file
	 *            - The {@link JarFile} of this plugin.
	 * @return the folder of the OS specific native libraries.
	 */
	private static String getLibFolder(JarFile file) {
		String libFolder = HDF_LIB_DIR;
		boolean any = false;
		for (String os : OS_NAMES) {
			if (file.getEntry(libFolder + "/" + os) != null) {
				any = true;
			}
			if (any) {
				break;
			}
		}
		if (!any) {
			return libFolder;
		}
		libFolder += "/";
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("win")) {
			os = "win";
		} else if (os.contains("mac")) {
			os = "mac";
		} else if (os.contains("nux") || os.contains("nix")
				|| os.contains("aix")) {
			os = "lin";
		} else if (os.contains("sunos") || os.contains("solaris")) {
			os = "sol";
		} else {
			// non supported OS
			return null;
		}
		if (os.equals("win")) {
			String bitNess = System.getProperty("sun.arch.data.model", "32");
			if (bitNess.equals("unknown")) {
				bitNess = "32";
			}
			os += bitNess;
		}
		return libFolder + os;
	}

	/**
	 * Copying a single library to the specified target if it is not already
	 * present. The idea is from <a
	 * href="http://snipplr.com/view/1744/extract-zipentry
	 * -into-a-fileoutputstream"
	 * />http://snipplr.com/view/1744/extract-zipentry-into
	 * -a-fileoutputstream/</a>.
	 *
	 * @param inputStream
	 *            - The library as an {@link InputStream}.
	 * @param target
	 *            - The target file to copy to.
	 * @throws Exception
	 *             if an error occurs.
	 */
	private static void copyLibFile(InputStream inputStream, File target)
			throws Exception {
		if (!target.createNewFile()) {
			return;
		}
		FileOutputStream fos = new FileOutputStream(target);
		byte[] data = new byte[1024];
		int length;
		while ((length = inputStream.read(data)) != -1) {
			fos.write(data, 0, length);
		}
		inputStream.close();
		fos.close();
	}

	/**
	 * Adds the specified path to the java library path if it is not already
	 * present. This solution is from <a
	 * href="http://fahdshariff.blogspot.jp/2011/08/changing
	 * -java-library-path-at
	 * -runtime.html">http://fahdshariff.blogspot.jp/2011/08/changing
	 * -java-library-path-at-runtime.html<a/>
	 *
	 * @param pathToAdd
	 *            the new library path
	 * @throws Exception
	 */
	private static boolean addLibraryPath(String pathToAdd) throws Exception {
		final Field usrPathsField = ClassLoader.class
				.getDeclaredField("usr_paths");
		usrPathsField.setAccessible(true);

		// get array of paths
		final String[] paths = (String[]) usrPathsField.get(null);

		// check if the path to add is already present
		for (String path : paths) {
			if (path.equals(pathToAdd)) {
				return false;
			}
		}

		// add the new path
		final String[] newPaths = new String[paths.length + 1];
		System.arraycopy(paths, 0, newPaths, 1, paths.length);
		// Arrays.copyOfRange(paths, paths.length + 1);
		// final String[] newPaths = Arrays.copy
		newPaths[0] = pathToAdd;
		usrPathsField.set(null, newPaths);
		return true;
	}

	/**
	 * This method is called during start up as the second hook. It is called
	 * before the gui of the mainframe is created. The Mainframe is given to
	 * adapt the gui. The operators and renderers have been registered in the
	 * meanwhile.
	 */
	public static void initGui(MainFrame mainframe) {
		// set operator icons according to RapidMiner standard (RM 7 and up)
		GroupTree daf = OperatorService.getGroups().getSubGroup("data_access");
		if (daf == null) {
			return;
		}
		daf = daf.getSubGroup("files");
		if (daf == null) {
			return;
		}
		String readIcon, writeIcon;
		readIcon = daf.getSubGroup("read").getIconName();
		writeIcon = daf.getSubGroup("write").getIconName();
		OperatorService.getOperatorDescription("hdf5:read_hdf5").setIconName(
				readIcon);
		OperatorService.getOperatorDescription("hdf5:write_hdf5").setIconName(
				writeIcon);
	}

	/**
	 * The last hook before the splash screen is closed. Third in the row.
	 */
	public static void initFinalChecks() {
		rep = new ResourceRepository("HDF5 Examples", "hdf5rep");
		RepositoryManager.getInstance(null).addRepository(rep);
	}

	/**
	 * Will be called as fourth method, directly before the UpdateManager is
	 * used for checking updates. Location for exchanging the UpdateManager. The
	 * name of this method unfortunately is a result of a historical typo, so
	 * it's a little bit misleading.
	 */
	public static void initPluginManager() {
	}

	/**
	 * Will be called when the plugin should be removed. This can happen on
	 * reloading for changes in the plugin or if the plugin should be disabled.
	 * In this method all additions to the GUI created by this plugin should be
	 * removed.
	 */
	public static void tearDownGUI(final MainFrame mainFrame) {
	}

	/**
	 * Will be called after tearDownGUI and should remove all non-GUI related
	 * additions this plugin made during initialization.
	 *
	 */
	public static void tearDown() {
		if (rep != null) {
			RepositoryManager.getInstance(null).removeRepository(rep);
		}
		if (removeLibPath) {
			try {
				removeLibraryPath(libDir.getAbsolutePath());
			} catch (Exception e) {
			}
			Field libs;
			// free native libraries
			try {
				libs = ClassLoader.class.getDeclaredField("nativeLibraries");
				libs.setAccessible(true);
				libs.set(PluginInitHDF5.class.getClassLoader(), new Vector<>());
			} catch (Exception e) {
			}
		}
	}

	/**
	 * Removes the specified path from the java library path if present. This
	 * method is the opposite of {@link #addLibraryPath(String)}.
	 *
	 * @param pathToRemove
	 *            - The path to remove.
	 * @throws Exception
	 *             if an error occurs.
	 */
	private static void removeLibraryPath(String pathToRemove) throws Exception {
		Field usrPathsField = ClassLoader.class.getDeclaredField("usr_paths");
		usrPathsField.setAccessible(true);

		// get array of paths
		String[] paths;
		paths = (String[]) usrPathsField.get(null);

		// get the index of the path to remove
		int i = 0;
		for (String path : paths) {
			if (path.equals(pathToRemove)) {
				break;
			}
			i++;
		}
		if (i == paths.length) {
			return;
		}

		// remove the path
		final String[] newPaths = new String[paths.length - 1];
		if (i > 0) {
			System.arraycopy(paths, 0, newPaths, 0, i);
		}
		if (i < newPaths.length) {
			System.arraycopy(paths, i + 1, newPaths, i, newPaths.length - i);
		}
		usrPathsField.set(null, newPaths);
	}

}
